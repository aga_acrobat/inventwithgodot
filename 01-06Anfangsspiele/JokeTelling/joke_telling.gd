extends Control

var intro = "You are in a land full of dragons. In front of you,
you see two caves. In one cave, the dragon is friendly
and will share his treasure with you. The other dragon
is greedy and hungry, and will eat you on sight.

Which cave will you go into? (1 or 2)"

@onready
var textfenster = %Textfenster

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	textfenster.text = intro

func _process(_delta: float) -> void:
	if Input.is_action_just_pressed("ui_accept"):
		pass
	if Input.is_action_pressed("ui_cancel"):
		get_tree().quit()
