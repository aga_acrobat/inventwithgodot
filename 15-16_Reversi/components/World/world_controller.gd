##
## World Controller Node
##
## manages game world related nodes
##
extends Node

var background: Control = preload("res://components/World/background.tscn").instantiate()
var game_board: TileMap = preload("res://components/World/game_board/board.tscn").instantiate()


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	add_child(background)
	add_child(game_board)
