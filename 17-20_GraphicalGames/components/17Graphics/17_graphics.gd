extends Node2D
##
## draws object for chapter 17 Creating graphics
##

@onready var background_rect: Rect2 = get_window().get_visible_rect()


# Called when CanvasItem has been requested to redraw.
func _draw() -> void:
	draw_background()
	draw_blue_circle()
	draw_green_polygon()
	draw_blue_lines()
	draw_red_elipse()
	draw_text()
	draw_pixel()


# fills viewport with white color
func draw_background() -> void:
	draw_rect(background_rect, Color.WHITE)


# draws blue circle
func draw_blue_circle() -> void:
	var circle_position: Vector2 = Vector2(300, 50)
	var radius: float = 20
	var color: Color = Color.BLUE
	draw_circle(circle_position, radius, color)


# draw green polygon
func draw_green_polygon() -> void:
	var polygon_color: Color = Color.GREEN
	var polygon_points: PackedVector2Array = PackedVector2Array(
		[
			Vector2(146, 0),
			Vector2(291, 106),
			Vector2(236, 277),
			Vector2(56, 277),
			Vector2(0, 106),
		]
	)
	draw_colored_polygon(polygon_points, polygon_color)


# draw three blue lines
func draw_blue_lines() -> void:
	var line_one: Dictionary = {
		start = Vector2(60, 60) as Vector2,
		end = Vector2(120, 60) as Vector2,
		color = Color.BLUE as Color,
		width = 4 as float
	}
	var line_two: Dictionary = {
		start = Vector2(120, 60) as Vector2,
		end = Vector2(60, 120) as Vector2,
		color = Color.BLUE as Color,
		width = 1 as float
	}
	var line_three: Dictionary = {
		start = Vector2(60, 120) as Vector2,
		end = Vector2(120, 120) as Vector2,
		color = Color.BLUE as Color,
		width = 4 as float
	}
	var lines: Array = [line_one, line_two, line_three]
	for line: Dictionary in lines:
		draw_line(
			line.start as Vector2, line.end as Vector2, line.color as Color, line.width as float
		)


# draw red elipse
func draw_red_elipse() -> void:
	var elipse_color: Color = Color.RED
	var elipse_center: Vector2 = Vector2(300, 300)
	var elipse_line_width: float = 1
	var elipse_width: int = 40
	var elipse_height: int = 80

	var p0: Vector2 = elipse_center - Vector2(roundi(elipse_width / 2.0), 0)
	var p1: Vector2 = elipse_center - Vector2(0, roundi(elipse_height / 2.0))
	var p2: Vector2 = elipse_center + Vector2(roundi(elipse_width / 2.0), 0)
	var p3: Vector2 = elipse_center + Vector2(0, roundi(elipse_height / 2.0))

	var ellipse_curve: Curve2D = Curve2D.new()
	# var control_point_x: int = roundi(elipse_width / 2.0)
	var control_point_x: int = 10
	ellipse_curve.add_point(p0)
	ellipse_curve.add_point(p1, Vector2(-control_point_x, 0), Vector2(control_point_x, 0))
	ellipse_curve.add_point(p2)
	ellipse_curve.add_point(p3, Vector2(control_point_x, 0), Vector2(-control_point_x, 0))
	ellipse_curve.add_point(p0)
	var ellipse_baked_points: PackedVector2Array = ellipse_curve.get_baked_points()
	draw_polyline(ellipse_baked_points, elipse_color, elipse_line_width)


# draw text Hello world! on surface with rectangular background
func draw_text() -> void:
	var font: Font = SystemFont.new()
	var font_size: int = 24
	var text_color: Color = Color.WHITE
	var text_background: Color = Color.BLUE
	var bg_color: Color = Color.RED
	var text: String = "Hello world!"

	var screen_center: Vector2 = background_rect.get_center()
	var text_size: Vector2 = font.get_string_size(text, HORIZONTAL_ALIGNMENT_CENTER, -1, font_size)
	var text_position: Vector2 = screen_center - (text_size / 2)
	text_position.y = text_position.y + font.get_ascent(font_size)

	var rect_start: Vector2 = Vector2.ZERO
	rect_start = Vector2(text_position.x, text_position.y - font.get_ascent(font_size))
	var text_rectangle: Rect2 = Rect2(rect_start, text_size)

	var bg_rect_start: Vector2 = rect_start - Vector2(20, 20)
	var bg_rect_size: Vector2 = text_size + Vector2(40, 40)
	var bg_rectangle: Rect2 = Rect2(bg_rect_start, bg_rect_size)

	draw_rect(bg_rectangle, bg_color)
	draw_rect(text_rectangle, text_background)
	draw_string(font, text_position, text, HORIZONTAL_ALIGNMENT_CENTER, -1, font_size, text_color)


# draw one black pixel
func draw_pixel() -> void:
	var points: PackedVector2Array = PackedVector2Array(
		[
			Vector2(480, 380),
		]
	)
	var colors: PackedColorArray = PackedColorArray(
		[
			Color.BLACK,
		]
	)

	draw_primitive(points, colors, PackedVector2Array())
