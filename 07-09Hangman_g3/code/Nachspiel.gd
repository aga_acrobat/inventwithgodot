extends Control

onready var status_box: HBoxContainer = $"%StatusBox"

## wenn das Spiel zuende ist
func _on_hangman_spiel_beendet(zustand: String) -> void:
	create_status_letters(zustand)


## Erstellt je eine Textur für jeden Buchstaben im Statusfeld
func create_status_letters(status: String) -> void:
	for buchstabe in status:
		var textur_pfad = "res://assets/buchstaben/" + buchstabe.to_upper() + ".png"
		var textur = load(textur_pfad)
		var buchstabenfeld = TextureRect.new()
		buchstabenfeld.texture = textur
		buchstabenfeld.stretch_mode = TextureRect.STRETCH_KEEP_CENTERED
		status_box.add_child(buchstabenfeld)


## wenn der Nocheinmal Button gedrückt wird
func _on_WiederholungsButton_pressed() -> void:
	var _err = get_tree().change_scene("res://code/VorSpiel.tscn")

