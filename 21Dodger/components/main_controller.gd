extends Node

var current_state: int
var current_score: float = 0.0:
	get:
		return _current_score
	set(value):
		_current_score = value
		SignalManager.current_score_changed.emit(int(_current_score))

var high_score: int = 0:
	get:
		return _high_score
	set(value):
		_high_score = value
		SignalManager.high_score_changed.emit(_high_score)

var _high_score: int = 0
var _current_score: float = 0.0

@onready var background_music: AudioStreamPlayer = $"BackgroundMusic"
@onready var gameover_music: AudioStreamPlayer = $"GameOverMusic"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	SignalManager.state_changed.connect(_on_state_changed_received)
	SignalManager.state_changed.emit(Constants.STATE.TITLE)
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)


# Called when an InputEventKey hasn't been consumed
func _unhandled_key_input(event: InputEvent) -> void:
	if current_state == Constants.STATE.GAME:
		if event.is_action_pressed("cheat_slow") || event.is_action_pressed("cheat_reverse"):
			current_score = 0
	elif current_state == Constants.STATE.TITLE || current_state == Constants.STATE.OVER:
		if event.is_pressed():
			SignalManager.state_changed.emit(Constants.STATE.GAME)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if current_state == Constants.STATE.GAME:
		current_score += delta


# called when the game state changes
func _on_state_changed_received(new_state):
	current_state = new_state
	match new_state:
		Constants.STATE.TITLE:
			game_start()
		Constants.STATE.OVER:
			game_over()
		Constants.STATE.GAME:
			game_start()


# called when title state is reached
func game_start():
	gameover_music.stop()
	background_music.play()


# called when the game over state is reached
func game_over():
	background_music.stop()
	gameover_music.play()
	reset_scores()


# called on game over to reset scores
func reset_scores():
	if current_score > high_score:
		high_score = int(current_score)
	current_score = 0
