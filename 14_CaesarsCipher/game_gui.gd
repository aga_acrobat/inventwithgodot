extends Control
@onready var key_label: Label = %KeyLabel
@onready var text_input: TextEdit = %TextInput
@onready var text_output: TextEdit = %TextOutput
@onready var text_bruteforce: TextEdit = %TextBF

var key_chart: String = " abcdefghijklmnopqrstuvwxyz.,-!§$%&/()=?1234567890öäüßQWERTZUIOPÜÄÖLKJHGFDSAYXCVBNM#*+'_:;<>"
var key: int = 0
var mode: int
var input: String

# Called when the node enters the scene tree for the first time.
func _ready():
	key_label.text = str(key)
	text_bruteforce.visible = false
	text_input.grab_focus()


func lettershift(letter: String) -> String:
	var letter_new: String = letter
	if letter in key_chart:
		var position_old: int = key_chart.find(letter)
		var position_new: int = position_old + key
		if position_new >= key_chart.length():
			position_new = position_new - key_chart.length()
		letter_new = key_chart[position_new]
	return letter_new


func text_loop(text: String) -> String:
	var text_new: String = ""
	for letter in text:
		var letter_new: String = lettershift(letter)
		text_new += letter_new
	return text_new


func crypt(text: String, type: int) -> String:
	var text_new: String = ""
	if type == 1:
		key = key * -1
	text_new = text_loop(text)
	return text_new


func brute_force_decrypt(text: String) -> String:
	var brute_force_text: String = ""
	for i in range(key_chart.length()):
		key = i
		brute_force_text += str(i) + ": "
		brute_force_text += text_loop(text)
		brute_force_text += "\n"
	return brute_force_text


func display_crypt() -> void:
	input = text_input.text
	text_output.text = crypt(input, mode)


func _on_up_button_pressed():
	key += 1
	key_label.text = str(key)


func _on_down_button_pressed():
	if key > 0:
		key -= 1
		key_label.text = str(key)


func _on_clear_button_pressed():
	text_input.clear()
	text_output.clear()
	key = 0
	key_label.text = str(key)
	text_bruteforce.visible = false


func _on_v_button_pressed():
	mode = 1
	display_crypt()


func _on_e_button_pressed():
	mode = 0
	display_crypt()


func _on_bf_button_pressed():
	text_bruteforce.text = brute_force_decrypt(text_input.text)
	text_bruteforce.visible = true
