extends Control

onready var wortlaengen_auswahl: OptionButton = $"%WortlaengenAuswahl"
onready var Global = get_node("/root/Global")


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	wortlaengen_auswahl.theme = Theme.new()
	wortlaengen_auswahl.theme.default_font = DynamicFont.new()
	wortlaengen_auswahl.theme.default_font.font_data = load("res://assets/fonts/IBMPlexSans-Regular.ttf")
	wortlaengen_auswahl.theme.default_font.size = 25
	
	var wortlaengen := ["egal", "kurz", "mittel", "lang"]
	for laenge in wortlaengen:
		wortlaengen_auswahl.add_item(laenge)


func _on_StartButton_pressed() -> void:
	Global.set_wortlaenge(wortlaengen_auswahl.selected)
	var _err = get_tree().change_scene("res://code/HauptSpiel.tscn")

