# TODO: docs
extends Control

var color: int

@onready var choice_light_button: TextureButton = $"%ChoiceLightBtn"
@onready var choice_dark_button: TextureButton = $"%ChoiceDarkBtn"


func _ready() -> void:
	SignalManager.restart_pressed.connect(_on_restart_button_pressed)


func _on_start_button_pressed() -> void:
	if color:
		SignalManager.start_pressed.emit()


func _on_choice_dark_btn_pressed() -> void:
	color = Constants.COLOR.DARK
	SignalManager.color_chosen.emit(color)


func _on_choice_light_btn_pressed() -> void:
	color = Constants.COLOR.LIGHT
	SignalManager.color_chosen.emit(color)


func _on_restart_button_pressed() -> void:
	match color:
		Constants.COLOR.LIGHT:
			choice_light_button.button_pressed = true
		Constants.COLOR.DARK:
			choice_dark_button.button_pressed = true
	SignalManager.color_chosen.emit(color)
