extends Node

signal current_score_changed(new_score)
signal high_score_changed(new_high_score)
signal state_changed(new_state)
