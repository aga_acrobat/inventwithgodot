extends Control

# constants from game settings
var lock_length: int
var clue_history: bool
var difficulty: int

# guess counters
var max_guesses: int
var num_guesses: int

# lock numbers
var secret_numbers: Array
var player_guesses: Array

var indicators: Array

# loaded ressources
@onready var Global = get_node("/root/Global")
@onready var LockScene = preload("res://code/main_game/lock_v_box.tscn")
@onready var HistoryEntry = preload("res://code/main_game/history_entry.tscn")
@onready var YellowIndicatorTexture: Texture2D = preload(
	"res://assets/zeichen/zeichen-gelb.png"
	)
@onready var RedIndicatorTexture: Texture2D = preload(
	"res://assets/zeichen/zeichen-rot.png"
	)
@onready var GreenIndicatorTexture: Texture2D = preload(
	"res://assets/zeichen/zeichen-gruen.png"
	)
@onready var BackgroundIndicatorTexture: Texture2D = preload(
	"res://assets/zeichen/IndicatorBckgrnd.png"
	)
@onready var RedCircleTexture: Texture2D = preload(
	"res://assets/zeichen/rot.png"
	)
@onready var GreenCircleTexture: Texture2D = preload(
	"res://assets/zeichen/gruen.png"
	)
@onready var YellowCircleTexture: Texture2D = preload(
	"res://assets/zeichen/gelb.png"
	)

# node references
@onready var IndicatorBox: HBoxContainer = $"%IndicatorHBox"
@onready var HistoryBox: VBoxContainer = $"%HistoryVBox"
@onready var LockBox: HBoxContainer = $"%LockHBox"
@onready var Indicator1: TextureRect =$"%IB-1"
@onready var Indicator2: TextureRect =$"%IB-2"
@onready var Indicator3: TextureRect =$"%IB-3"

# Called when the node enters the scene tree for the first time.
func _ready():
	lock_length = Global.lock_length
	clue_history = Global.clue_history
	difficulty = Global.difficulty
	num_guesses = 0

	randomize()
	load_locks(lock_length)
	set_max_guesses()
	create_secret_number()
	indicators.append(Indicator1)
	indicators.append(Indicator2)
	indicators.append(Indicator3)
	prints("zahlen: ", secret_numbers)


# translates difficulty settings into maximum number of available guesses
func set_max_guesses() -> void:
	match difficulty:
		1:
			max_guesses = -1
		2:
			max_guesses = 10
		3:
			max_guesses = 8


# instantiates given number of lock scenes
func load_locks(number_locks: int) -> void:
	for x in range(number_locks):
		var lock: Control = LockScene.instantiate()
		LockBox.add_child(lock)


# creates secret number
func create_secret_number() -> void:
	var numbers: Array = range(10)
	numbers.shuffle()
	var random_numbers: Array = []
	for i in range(lock_length):
		random_numbers.append(numbers[i])
	secret_numbers = random_numbers
	Global.secret_numbers = random_numbers


# checks win, loss, clues
func _on_ok_button_pressed():
	num_guesses += 1
	player_guesses.clear()
	for box in LockBox.get_children():
		player_guesses.append(box.current_texture_nr)
	process_player_guess()


# process player input
func process_player_guess() -> void:
	var _clues: Array
	if check_win():
		win()
	elif max_guesses != -1 and num_guesses >= max_guesses:
		lost()
	else:
		_clues = check_clues()
		display_clues(_clues)
		if clue_history:
			create_history_entry(_clues)


# checks if player has won
func check_win() -> bool:
	if player_guesses == secret_numbers:
		return true
	return false


# checks which clues to give to the player
func check_clues() -> Array:
	var clues: Array = []

	for n in range(player_guesses.size()):
		if player_guesses[n] == secret_numbers[n]:
			clues.append(1)
		elif player_guesses[n] in secret_numbers:
			clues.append(2)
	
	if clues.size() == 0:
		clues.append(0)

	clues.sort()
	return clues


# display clues to the player
func display_clues(clues: Array) -> void:
	var indicator_settings: Array
	for n in range(indicators.size()):
		if clues.size() >= n+1:
			indicator_settings = match_clue_type(clues[n])
		else:
			indicator_settings = match_clue_type(-1)
		indicators[n].tooltip_text = indicator_settings[0]
		indicators[n].texture = indicator_settings[1]


# return tooltip and texture for given clue type
func match_clue_type(type: int) -> Array:
	match type:
		0:
			return ["alles falsch", RedIndicatorTexture]
		1:
			return ["eine Zahl ist KOMPLETT richtig geraten", GreenIndicatorTexture]
		2:
			return ["eine Zahl ist richtig geraten, aber an der FALSCHEN Stelle", YellowIndicatorTexture]
	return ["", BackgroundIndicatorTexture]


# create entry in history list
func create_history_entry(clues: Array) -> void:
	var entry: RichTextLabel = HistoryEntry.instantiate()
	for clue in clues:
		match clue:
			0:
				entry.append_text("🔴 ")
			1:
				entry.append_text("🟢 ")
			2:
				entry.append_text("🟡 ")
	for digit in player_guesses:
		entry.append_text(str(digit))
	HistoryBox.add_child(entry)
	HistoryBox.move_child(entry, 0)


func match_clue_type2(type: int) -> String:
	prints(type)
	return ""


func win() -> void:
	Global.state = "GEWONNEN!"
	get_tree().change_scene_to_file("res://code/post_game/post_game_gui.tscn")


func lost() -> void:
	Global.state = "VERLOREN!"
	get_tree().change_scene_to_file("res://code/post_game/post_game_gui.tscn")


## Beendet das Spiel mit der Esc-Taste
func _process(_delta: float) -> void:
	if Input.is_action_pressed("ui_cancel"):
		get_tree().quit()
