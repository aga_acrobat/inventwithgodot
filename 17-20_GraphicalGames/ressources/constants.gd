class_name Constants
extends RefCounted

const UPLEFT: Vector2 = Vector2(-1, -1)
const DOWNLEFT: Vector2 = Vector2(-1, 1)
const UPRIGHT: Vector2 = Vector2(1, -1)
const DOWNRIGHT: Vector2 = Vector2(1, 1)
