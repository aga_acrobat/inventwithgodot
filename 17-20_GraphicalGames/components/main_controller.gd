extends Node
##
## main controller node
##

## preload scenes and instantiate
@onready var graphics_window: Window = $"17Graphics"
@onready var animation_window: Window = $"18Animation"
@onready var collision_window: Window = $"19Collision"
@onready var sounds_window: Window = $"20SpritesAndSounds"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	set_windows()


# catch key events
func _unhandled_key_input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_cancel"):
		get_tree().quit()


func set_windows() -> void:
	graphics_window.position = Vector2(50, 50)
	graphics_window.size = Vector2(500, 400)

	animation_window.position = Vector2(600, 50)
	animation_window.size = Vector2(400, 400)

	collision_window.position = Vector2(50, 600)
	collision_window.size = Vector2(400, 400)

	sounds_window.position = Vector2(600, 600)
	sounds_window.size = Vector2(400, 400)
