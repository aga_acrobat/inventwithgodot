extends Control
# Hauptszene für das Galgenmännchen Spiel

signal spiel_beendet(zustand)

var alphabet := "abcdefghijklmnopqrstuvwxyzäöüß"
var wortliste := ["abend", "abendessen", "advent", "affe", "afrika", "ameise", "amerika", "ampel", "amsel", "anfang", "angst", "apfel", "april", "aquarium", "arm", "arzt", "asien", "ast", "aufgabe", "auge", "august", "australien", "auto", "axt", "baby", "bach", "bäcker", "bad", "ball", "banane", "band", "bank", "bär", "bauch", "baum", "beere", "bein", "beispiel", "berg", "besen", "bett", "biber", "biene", "birne", "blume", "blüte", "boot", "brief", "brille", "brot", "brücke", "bruder", "buch", "burg", "bus", "butter", "cent", "chor", "clown", "comic", "computer", "dach", "dachs", "daumen", "decke", "detektiv", "dezember", "dieb", "dienstag", "donnerstag", "dose", "drache", "durst", "dusche", "ei", "eimer", "eis", "elefant", "eltern", "e-mail", "ende", "engel", "enkel", "ente", "erdbeere", "erde", "ereignis", "erfolg", "ergebnis", "erklärung", "erlebnis", "erzählung", "esel", "eule", "europa", "fach", "faden", "fahrrad", "farbe", "faust", "februar", "feder", "fehler", "feld", "fenster", "ferien", "fernseher", "fest", "feuerwehr", "fieber", "film", "finger", "fisch", "flasche", "fleck", "fliege", "flosse", "flöte", "flugzeug", "fluss", "frage", "frau", "freiheit", "freitag", "freude", "freund", "frosch", "frucht", "frühling", "frühstück", "fuchs", "füller", "fuß", "fußball", "gabel", "garten", "gast", "gebäude", "geburt", "geburtstag", "geheimnis", "geld", "gemüse", "geschenk", "geschichte", "gesicht", "gesundheit", "gewitter", "giraffe", "glas", "glück", "gras", "gruppe", "gruß", "haar", "hai", "hals", "hamster", "hand", "handy", "hase", "haus", "haut", "heft", "heizung", "herbst", "herr", "herz", "hexe", "himmel", "hitze", "hobby", "hoffnung", "höhle", "hose", "huhn", "hummel", "hund", "hunger", "idee", "igel", "impfung", "insel", "jacke", "jahr", "januar", "juli", "junge", "juni", "käfer", "käfig", "kaiser", "kälte", "karte", "karten", "käse", "kater", "katze", "kerze", "kind", "kino", "kirche", "kirsche", "kissen", "klasse", "kleid", "kleidung", "knie", "knopf", "koffer", "kopf", "korb", "körper", "krankheit", "kreuz", "kreuzung", "krone", "küche", "kuh", "lampe", "land", "länge", "lärm", "laub", "lehrer", "leiter", "lexikon", "licht", "lied", "lob", "loch", "löffel", "löwe", "luchs", "mädchen", "mai", "mais", "mama", "mandarine", "mann", "mannschaft", "mantel", "mappe", "märchen", "märz", "maschine", "maus", "meer", "meinung", "messer", "meter", "milch", "minute", "mittag", "mittagessen", "mitte", "mittwoch", "mond", "montag", "moos", "mühe", "müll", "mund", "muschel", "mutter", "mütze", "nachbar", "nachmittag", "nacht", "nadel", "nähe", "nahrung", "name", "nase", "nashorn", "nest", "note", "november", "nudel", "nuss", "obst", "ohr", "oktober", "öl", "oma", "onkel", "opa", "ordnung", "ostern", "paar", "päckchen", "papa", "papagei", "papier", "pappe", "party", "pfeil", "pferd", "pflanze", "pflaster", "pfote", "pfütze", "pinsel", "pizza", "platz", "pony", "preis", "pullover", "punkt", "puppe", "pyramide", "qu", "quader", "quadrat", "qualm", "quatsch", "quelle", "rad", "rätsel", "räuber", "rauch", "raum", "rechnung", "regen", "reh", "reihe", "reise", "rettung", "richtung", "ring", "rock", "rose", "rücken", "ruhe", "saal", "sache", "saft", "säge", "samstag", "satz", "schaf", "schal", "schatz", "schaufel", "schaukel", "schere", "schiff", "schlange", "schlitten", "schloss", "schluss", "schlüssel", "schmetterling", "schnecke", "schnee", "schneemann", "schnupfen", "schrank", "schreck", "schreibtisch", "schuh", "schule", "schwein", "schwester", "schwierigkeit", "schwimmbad", "see", "seife", "seite", "september", "silvester", "socke", "sohn", "sommer", "sonne", "sonntag", "spaß", "spaten", "spatz", "spiegel", "spiel", "spinne", "spitze", "sport", "sprache", "sprung", "stadt", "stamm", "stärke", "stein", "stempel", "stern", "stiefel", "stift", "stoff", "strafe", "strand", "straße", "streifen", "strom", "stück", "stufe", "stuhl", "stunde", "sturm", "suppe", "süßigkeit", "tafel", "tag", "tante", "tasche", "tasse", "taxi", "teddy", "tee", "teller", "text", "theater", "thema", "thermometer", "tier", "tiger", "tipp", "tisch", "tochter", "tod", "topf", "tor", "träne", "traum", "treppe", "tür", "übung", "uhr", "uhu", "ungeheuer", "urlaub", "vampir", "vase", "vater", "verein", "verkäufer", "verwandtschaft", "virus", "vogel", "vorfahrt", "vorname", "vorschlag", "vorteil", "vulkan", "waage", "wald", "wand", "wärme", "wäsche", "wasser", "wecker", "weg", "weihnachten", "weile", "welt", "wetter", "wiese", "winter", "witz", "woche", "wohnung", "wolke", "wort", "würfel", "wurm", "wurst", "zahn", "zaun", "zeh", "zeichnung", "zeit", "zeitung", "zelt", "zeugnis", "ziege", "zimmer", "zitrone", "zoo", "zucker", "zug", "zukunft", "zunge", "zwerg", "zylinder"]
var wort: String
var erraten := 0

onready var Global = get_node("/root/Global")
onready var Button_Szene = preload("res://code/BuchstabenKnopf.tscn")
onready var wort_buchstaben: HBoxContainer = $"%WortBox"
onready var hangman_textur: TextureRect = $"%HangmanTextur"
onready var buchstaben_netz: GridContainer = $"%Buchstabennetz"
onready var nachspiel: Control = $Nachspiel


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	randomize()
	fill_letter_grid()
	restart()

## Füllt das Buchstabennetz mit Buchstabenknöpfen A-Z
func fill_letter_grid() -> void:
	for buchstabe in alphabet:
		var button = create_letter_button(buchstabe.to_upper())
		buchstaben_netz.add_child(button)


## Resettet das Spielfeld bei Spielneustart.
func restart() -> void:
	# versteckt das EndspielOverlay
	nachspiel.visible = false

	#Wortfelder leeren
	var wort_elemente = wort_buchstaben.get_children()
	for feld in wort_elemente:
		feld.queue_free()
	
	#hangmansprite auf anfang setzen
	hangman_textur.texture.current_frame = 0
	
	randomize_word()
	create_word_letters()

	#buchstabenbuttons aktivieren
	for buchstabe in buchstaben_netz.get_children():
		buchstabe.disabled = false
	
	erraten = 0


## erstellt für den gegebenen Buchstaben einen Button
func create_letter_button(buchstabe: String) -> Button:
	var icon_pfad = "res://assets/buchstaben/" + buchstabe + ".png"
	var buchstabenButton: BuchstabenKnopf = Button_Szene.instance()
	buchstabenButton.buchstabe = buchstabe
	buchstabenButton.icon_align = 1
	buchstabenButton.icon = load(icon_pfad)
	var _err = buchstabenButton.connect("pressed", self, "_on_letterbutton_pressed", [buchstabenButton])
	return buchstabenButton


## Randomisiert das gesuchte Wort nach gewählter Schwierigkeit
func randomize_word() -> void:
	#wort ranodmisieren und Leerfelder setzen
	var wort_laenge: Array = Global.wortlaenge
	var zufallswort: String

	while wort.length() == 0:
		zufallswort = wortliste[randi() % wortliste.size()]
		if (
			zufallswort.length() >= wort_laenge[0] 
			and zufallswort.length() <= wort_laenge[1]
			):
			wort = zufallswort


## erstellt für jeden Buchstaben im zu erratenden Wort ein zufällig gefärbtes
## Texturfeld und fügt es dem Wortfeld-BoxContainer hinzu
func create_word_letters() -> void:
	for buchstabe in wort:
		var farben =["blau", "gelb", "gruen", "lila", "rot"]
		var feld_farbe = farben[randi() % farben.size()]
		var textur_pfad = (
			"res://assets/leerfelder/" 
			+ feld_farbe 
			+ ".png")
		var textur: StreamTexture = load(textur_pfad)
		var buchstabenfeld: TextureRect = TextureRect.new()
		buchstabenfeld.texture = textur
#		buchstabenfeld.stretch_mode = TextureRect.STRETCH_KEEP_CENTERED
		buchstabenfeld.stretch_mode = TextureRect.STRETCH_SCALE_ON_EXPAND
		wort_buchstaben.add_child(buchstabenfeld)


## Ersetzt ein Texturfeld in der WortBox durch die passende Textur des 
## gegebenen Buchstaben
func show_letter(buchstabe: String) -> void:
	var buchstaben_felder = wort_buchstaben.get_children()
	var anzahl = wort.count(buchstabe)
	var anfang = 0
	for i in anzahl:
		var buchstaben_position = wort.find(buchstabe, anfang)
		var textur_pfad = (
			"res://assets/buchstaben/" 
			+ buchstabe.to_upper()
			+ ".png")
		buchstaben_felder[buchstaben_position].texture = load(textur_pfad)
		anfang = buchstaben_position + 1
		erraten += 1


## Prüft den Spielstatus und löst ggf. den Spielendzustand aus
func check_endstate():
	if erraten == wort.length():
		endstate("gewonnen")
	elif (
		hangman_textur.texture.current_frame 
		== hangman_textur.texture.frames - 1):
		endstate("verloren")


## Zeigt das zu erratende Wort an, blendet das Endspielpanel ein
func endstate(zustand: String):
	for buchstabe in wort:
		show_letter(buchstabe)
	emit_signal("spiel_beendet", zustand)
	nachspiel.visible = true


## Setzt den richtig geratenen Buchstaben ODER die HangmanTextur weiter
func _on_letterbutton_pressed(button: BuchstabenKnopf) -> void:
	var buchstabe: String = button.buchstabe.to_lower()
	button.disabled = true
	# falls der Buchstabe im zu erratenden Wort vorkommt
	if buchstabe in wort:
		show_letter(buchstabe)
	else:
		hangman_textur.texture.current_frame += 1
	check_endstate()


