extends Node2D
##
## draws objects for chapter 18 Animation
##

const BACKGROUND_COLOR: Color = Color.WHITE
const UPLEFT: Vector2 = Vector2(-1, -1)
const DOWNLEFT: Vector2 = Vector2(-1, 1)
const UPRIGHT: Vector2 = Vector2(1, -1)
const DOWNRIGHT: Vector2 = Vector2(1, 1)

@onready var background_rect: Rect2 = get_window().get_visible_rect()


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	create_boxes()


# Called when CanvasItem has been requested to redraw.
func _draw() -> void:
	draw_background()


# fills window with white color
func draw_background() -> void:
	draw_rect(background_rect, BACKGROUND_COLOR)


# creates three Box objects and adds them to the scene
func create_boxes() -> void:
	add_child(Box.new(Vector2(300, 80), Vector2(50, 100), Color.RED, UPRIGHT))
	add_child(Box.new(Vector2(200, 200), Vector2(20, 20), Color.GREEN, UPLEFT))
	add_child(Box.new(Vector2(100, 150), Vector2(60, 60), Color.BLUE, DOWNLEFT))
