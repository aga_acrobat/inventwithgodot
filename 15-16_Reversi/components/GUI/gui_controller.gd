##
## GUI Controller Node
##
## manages GUI related nodes
##
extends Node

## preload game screens and instantiate
var title_screen: Control = (
	preload("res://components/GUI/TitleScreen/title_screen.tscn").instantiate()
)
var main_screen: Control = preload("res://components/GUI/MainScreen/main_screen.tscn").instantiate()
var end_screen: Control = preload("res://components/GUI/EndScreen/end_screen.tscn").instantiate()


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	SignalManager.start_pressed.connect(_on_start_pressed_received)
	SignalManager.endgame_reached.connect(_on_endgame_reached_received)
	SignalManager.restart_pressed.connect(_on_restart_pressed_received)

	add_child(end_screen)
	add_child(main_screen)
	add_child(title_screen)
	remove_child(end_screen)


# TODO: docs!
func _on_start_pressed_received() -> void:
	remove_child(title_screen)


# TODO: docs!
func _on_endgame_reached_received() -> void:
	remove_child(main_screen)
	add_child(end_screen)


# TODO: docs!
func _on_restart_pressed_received() -> void:
	remove_child(end_screen)
	add_child(main_screen)
	add_child(title_screen)
