extends Control

@onready var status_box = %StatusBox

func _process(_delta: float) -> void:
	if Input.is_action_pressed("ui_cancel"):
		get_tree().quit()


func _on_hangman_spiel_beendet(zustand: String) -> void:
	# neue texturenrect erstellen für buchstaben in status
	create_status_letters(zustand)

## Erstellt je ein TexturRect für jeden Buchstaben im status und füllt es mit der passenden Textur
func create_status_letters(status: String) -> void:
	for buchstabe in status:
		var textur_pfad = "res://assets/buchstaben/" + buchstabe.to_upper() + ".png"
		var textur = load(textur_pfad)
		var buchstabenfeld = TextureRect.new()
		buchstabenfeld.texture = textur
		buchstabenfeld.stretch_mode = TextureRect.STRETCH_KEEP_CENTERED
		status_box.add_child(buchstabenfeld)

## wenn der Nocheinmal Button gedrückt wird
func _on_widerholung_button_pressed() -> void:
	get_tree().change_scene_to_file("res://code/Vorspiel.tscn")

## wenn der Ende Button gedrückt wird
func _on_ende_button_pressed() -> void:
	get_tree().quit()

