extends Node

var board_layer_tiles := 1
var tile_set_id := 0
var light_tile_atlas_coords := Vector2i(0, 1)
var dark_tile_atlas_coords := Vector2i(1, 1)
var hint_tile_atlas_coords := Vector2i(0, 2)

var DIRECTIONS := [
	Vector2i(0, 1),
	Vector2i(1, 1),
	Vector2i(1, 0),
	Vector2i(1, -1),
	Vector2i(0, -1),
	Vector2i(-1, -1),
	Vector2i(-1, 0),
	Vector2i(-1, 1),
	]

@onready var Spielfeld: TileMap = $"%TileMap"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

## Prüft, ob ein gegebenes Feld (tile) für die gegebene Farbe einen gültigen Zug ausmacht.
## Gibt ein Array an Spielsteinen zurück, die das gegebene Feld wandeln würde.
func check_valid_tile(tile: Vector2i, opponent: Dictionary) -> Array:
	var tiles_to_turn: Array = []
	# ist das gegeben Feld besetzt?
	if tile in Spielfeld.get_used_cells(board_layer_tiles):
		return tiles_to_turn
	# befindet sich das Feld in der Nähe eines gegenerischen Spielsteins
	var opponent_tiles: Array = Spielfeld.get_used_cells_by_id(
		board_layer_tiles, tile_set_id, opponent.atlas_coords
		)
	for op_tile in opponent_tiles:
		for d in DIRECTIONS:
			if tile == (op_tile + d):
				# wechselt das Feld mindestens einen Spielstein
				tiles_to_turn = calculate_turn_tiles(tile, d)
				return tiles_to_turn
	return tiles_to_turn


func calculate_turn_tiles(clicked_tile: Vector2i, direction: Vector2i) -> Array:
	var tiles_to_turn: Array = []
	var distance: int = 1
	direction = direction * -1
	#while true:
		##Feld vom angeklicktem Feld aus in gegebener Richtung und Entfernung
		#var tile_to_check: Vector2i = clicked_tile + direction * distance
		#var tile_color_coords:Vector2i = Spielfeld.get_cell_atlas_coords(
			#board_layer_tiles, tile_to_check)
		## ist das Prüffeld leer?
		#if tile_to_check not in Spielfeld.get_used_cells(board_layer_tiles):
			#prints("leeres feld: ", tile_to_check)
			#return []
		## liegt auf dem Prüffeld ein gegnerischer Stein?
		#elif tile_color_coords == ai.atlas_coords:
			#prints("gegner feld: ", tile_to_check)
			#tiles_to_turn.append(tile_to_check)
			#distance += 1
		## liegt auf dem Prüffeld ein eigener Stein?
		#elif tile_color_coords == player.atlas_coords:
			#prints("eigenes feld: ", tile_to_check)
			#tiles_to_turn.append(tile_to_check)
			#tiles_to_turn.append(clicked_tile)
			#return tiles_to_turn
	return tiles_to_turn
