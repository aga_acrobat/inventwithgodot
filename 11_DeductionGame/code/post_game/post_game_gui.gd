extends Control

@onready var Global: Node = get_node("/root/Global")
@onready var StatusLabel: Label = $"%StatusLabel"
@onready var DigitBox: HBoxContainer = $"%DigitBox"
@onready var DigitScene: PackedScene = preload(
	"res://code/pre_game/pre_game_digit_texture_rect.tscn"
	)

# Called when the node enters the scene tree for the first time.
func _ready():
	StatusLabel.text = Global.state
	var numbers = Global.secret_numbers

	for number in numbers:
		var texture_path: String = "res://assets/zahlen/" + str(number) + ".png"
		var digit_texture: TextureRect = DigitScene.instantiate()
		digit_texture.texture = load(texture_path)
		DigitBox.add_child(digit_texture)


func _on_x_button_pressed():
	get_tree().quit()


func _on_v_button_pressed():
	get_tree().change_scene_to_file("res://code/pre_game/pre_game_gui.tscn")


## Beendet das Spiel mit der Esc-Taste
func _process(_delta: float) -> void:
	if Input.is_action_pressed("ui_cancel"):
		get_tree().quit()
