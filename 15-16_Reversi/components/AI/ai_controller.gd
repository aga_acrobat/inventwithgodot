##
## AI Controller Node
##
## game AI
## calculates AI moves in game
##
extends Node

enum STRATEGY { SIMPLE, MIDDLE, HARD, EXTRA }

var possible_ai_rows: Array
var ai_corner_cells: Array
var chosen_strategy: int = STRATEGY.SIMPLE


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	SignalManager.moves_calculated.connect(_on_moves_calculated)
	SignalManager.turn_changed.connect(_on_turn_changed)
	SignalManager.strategy_toggled.connect(set_strategy)


func set_strategy(level: int) -> void:
	match level:
		0:
			chosen_strategy = STRATEGY.SIMPLE
		1:
			chosen_strategy = STRATEGY.MIDDLE
		2:
			chosen_strategy = STRATEGY.HARD
		3:
			chosen_strategy = STRATEGY.EXTRA


func strategy_simple() -> Array:
	var best_move: Array = []
	best_move = possible_ai_rows.pick_random()
	return best_move


func strategy_middle() -> Array:
	var best_move: Array = []
	best_move = pick_longest_row()
	return best_move


func strategy_hard() -> Array:
	var best_move: Array = []
	best_move = pick_corner_cell(true)
	if not best_move:
		best_move = pick_longest_row()
	return best_move


func strategy_extra() -> Array:
	var best_move: Array = []
	best_move = pick_corner_cell(false)
	if not best_move:
		best_move = pick_longest_row()
	return best_move


# assigns moves calculated by the game board to local variables
func _on_moves_calculated(
	_player_rows: Array, ai_rows: Array, _player_corners: Array, ai_corners: Array
) -> void:
	possible_ai_rows = ai_rows
	ai_corner_cells = ai_corners


# chooses AI move on AI turn
func _on_turn_changed(actor: int) -> void:
	if actor == Constants.ACTOR.AI:
		choose_move()


# chooses AI move depending on set strategy level
func choose_move() -> void:
	var chosen_row: Array = []
	match chosen_strategy:
		STRATEGY.SIMPLE:
			chosen_row = strategy_simple()
		STRATEGY.MIDDLE:
			chosen_row = strategy_middle()
		STRATEGY.HARD:
			chosen_row = strategy_hard()
		STRATEGY.EXTRA:
			chosen_row = strategy_extra()
	SignalManager.ai_move_chosen.emit(chosen_row)


# helper function
# returns longest row as AI move
func pick_longest_row() -> Array:
	var longest_row: Array = []
	# if possible_ai_rows.size() < 2:
	# 	prints("ai controller: possible ai rows ", possible_ai_rows)
	possible_ai_rows.sort()
	assert(possible_ai_rows, "no possible ai rows")
	longest_row = possible_ai_rows[0]
	return longest_row


func pick_corner_cell(random: bool) -> Array:
	var corner_row: Array = []
	if ai_corner_cells:
		var corner_cell: Vector2i = Vector2i()
		if random:
			corner_cell = ai_corner_cells.pick_random()
		else:
			var corner_rows: Array = []
			for cell: Vector2i in ai_corner_cells:
				for row: Array in possible_ai_rows:
					if cell in row:
						corner_rows.append(row)
			corner_rows.sort()
			corner_row = corner_rows[0]
		for row: Array in possible_ai_rows:
			if corner_cell in row:
				corner_row = row
	return corner_row
