extends Reference
class_name Libraries

var spielfelder: Array = [0, 1, 2, 3, 4, 5, 6, 7, 8]
var gewinn_zeilen: Array = [
	[0, 1, 2],
	[3, 4, 5],
	[6, 7, 8],
	[0, 3, 6],
	[1, 4, 7],
	[2, 5, 8],
	[0, 4, 8],
	[2, 4, 6],
	]
var ecken_felder: Array = [0, 2, 6, 8]
var _spiel_knoepfe: Array


# Klassenkonstruktor
func _init(spiel_knoepfe: Array) -> void:
	assert(spiel_knoepfe[0].is_class("Button"))
	_spiel_knoepfe = spiel_knoepfe


## Findet irgendein freies Feld im Spielfeld
func find_free_field_in_board() -> int:
	return find_free_field(spielfelder)


## Findet ein freies Eckenfeld
func find_free_corner_field() -> int:
	return find_free_field(ecken_felder)


## Findet das erste freie Feld in der gegebenen Liste an Spielpositionen
func find_free_field(positions_liste: Array) -> int:
	for position in positions_liste:
		if _spiel_knoepfe[position].wert == -5:
			return position
	return -1


## Findet alle freien Felder in der gegebenen Liste an Spielpositionen
func find_free_fields(positions_liste: Array = spielfelder) -> Array:
	var freie_positionen: Array = []
	for position in positions_liste:
		if _spiel_knoepfe[position].wert == -5:
			freie_positionen.append(position)
	return freie_positionen


## Gibt ein Feld zurück, mit dem der gegebene Acteur gewinnen kann
func find_actor_win(actor: int) -> int:
	var gewinn_summe: int
	match actor:
		0: #ai
			gewinn_summe = -5
		1: #spieler
			gewinn_summe = -3
	var actor_win: Array = check_zeilen_summen(gewinn_summe, gewinn_zeilen)
	if actor_win:
		return find_free_field(actor_win)
	return -1


## Prüft, ob die Werte in einer Liste an gegebenen Zeilen die gegebene
## Zielsumme erreichen. Gibt ein Array mit der Zeile, die die Zeilensumme
## erreicht, oder ein leeres Array zurück.
func check_zeilen_summen(summe: int, zeilen_liste: Array) -> Array:
	var spiel_feld: Array = create_game_field()

	for zeile in zeilen_liste:
		var zeilen_summe := 0
		for index in zeile:
			zeilen_summe += spiel_feld[index]
		if zeilen_summe == summe:
			return zeile
	return []


## Erstellt ein SpielfeldArray
func create_game_field() -> Array:
	var spiel_feld: Array = []
	for knopf in _spiel_knoepfe:
		spiel_feld.append(knopf.wert)
	return spiel_feld


