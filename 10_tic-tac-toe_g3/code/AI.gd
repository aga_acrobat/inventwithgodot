extends Reference
class_name AI
# KI für das TicTacToe-Spiel

var _spiel_knoepfe: Array
var _schwierigkeit: int
var ai_cleverness: Dictionary = {
	"schlecht": 90,
	"gut": 80,
	"sehr gut": 40,
	}

var bibliothek: Libraries


# Klassenkonstruktor
func _init(spiel_knoepfe: Array, schwierigkeit: int) -> void:
	assert(spiel_knoepfe[0].is_class("Button"))
	_spiel_knoepfe = spiel_knoepfe
	bibliothek = Libraries.new(_spiel_knoepfe)
	match schwierigkeit:
		0:
			_schwierigkeit = ai_cleverness.schlecht
		1:
			_schwierigkeit = ai_cleverness.gut
		2:
			_schwierigkeit = ai_cleverness["sehr gut"]
	#prints("gewählte schwierigkeitsgrenze liegt bei: ", _schwierigkeit, " %")


## Berechnet den nächsten Zug der AI
func calculate_move() -> int:
	var wurf: float = randi() % 100

	if wurf <= _schwierigkeit:
		#prints("Zufallswurf liegt bei: ", wurf, "Computer verfolgt ZUFALLSFELD")
		return minimal_strategie()
	else:
		#prints("Zufallswurf liegt bei: ", wurf, "Computer verfolgt INTELLIGENTE strategie")
		return maximal_strategie()


## Minimalstrategie der AI
func minimal_strategie() -> int:
	var freie_felder: Array = bibliothek.find_free_fields()
	var random_free_field: int = freie_felder[randi() % freie_felder.size()]
	return random_free_field


## Maximalstrategie für die AI
func maximal_strategie() -> int:
	var strategien: Dictionary = possible_strategies()

	 #kann die AI gewinnen?, dann feld setzen
	if strategien.ai_win != -1:
		return strategien.ai_win
	 #kann der Spieler gewinnen?, dann feld setzen
	elif strategien.player_win != -1:
		return strategien.player_win
	 #ist das mittelfeld frei?, dann Feld setzen
	elif _spiel_knoepfe[4].wert == -5:
		return 4
	 #ist ein Eckfeld frei?, dann Feld setzen
	elif strategien.freies_eckenfeld != -1:
		return strategien.freies_eckenfeld
	 #ist irgendein FEld frei?, dann Feld setzen
	elif strategien.freies_feld != -1:
		return strategien.freies_feld

	return -1


## Mögliche Strategien
func possible_strategies() -> Dictionary:
	var strategies: Dictionary = {}
	strategies.ai_win = bibliothek.find_actor_win(0)
	strategies.player_win = bibliothek.find_actor_win(1)
	strategies.freies_eckenfeld = bibliothek.find_free_corner_field()
	strategies.mittelfeld = 4
	strategies.freies_feld = minimal_strategie()

	return strategies
