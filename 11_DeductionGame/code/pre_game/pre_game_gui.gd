extends Control

var lock_length: int
var clue_history: bool
var difficulty: int

@onready var Global: Node = get_node("/root/Global")
@onready var DigitsHBox: HBoxContainer = $"%DigitsHBox"
@onready var DigitScene: PackedScene = preload(
	"res://code/pre_game/pre_game_digit_texture_rect.tscn"
	)

# Called when the node enters the scene tree for the first time.
func _ready():
	lock_length = 3
	clue_history = true
	difficulty = 1
	update_lock_length()


func _on_start_button_pressed():
	Global.lock_length = lock_length
	Global.difficulty = difficulty
	Global.clue_history = clue_history
	Global.state = ""
	Global.secret_numbers = []
	get_tree().change_scene_to_file("res://code/main_game/main_game_gui.tscn")


func _on_hard_button_pressed():
	prints("hard")
	difficulty = 3


func _on_standard_button_pressed():
	prints("standard")
	difficulty = 2


func _on_easy_button_pressed():
	prints("easy")
	difficulty = 1


func _on_v_button_pressed():
	prints("v")
	clue_history = true


func _on_x_button_pressed():
	prints("x")
	clue_history = false


func _on_minus_texture_pressed():
	if lock_length > 1:
		lock_length -=1
		update_lock_length()

func _on_plus_button_pressed():
	if lock_length < 6:
		lock_length +=1
		update_lock_length()


func update_lock_length() -> void:
	for child in DigitsHBox.get_children():
		child.queue_free()
	for i in range(lock_length):
		var texture_path: String = "res://assets/zahlen/" + str(i+1) + ".png"
		var digit_texture: TextureRect = DigitScene.instantiate()
		digit_texture.texture = load(texture_path)
		DigitsHBox.add_child(digit_texture)


## Beendet das Spiel mit der Esc-Taste
func _process(_delta: float) -> void:
	if Input.is_action_pressed("ui_cancel"):
		get_tree().quit()


func _on_rules_button_pressed():
	pass # Replace with function body.
