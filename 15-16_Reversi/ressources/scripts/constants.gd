class_name Constants
extends RefCounted

enum COLOR {NONE, LIGHT, DARK}
enum ACTOR {NONE, PLAYER, AI}
