extends Area2D
#
# FoodBox
#

var size: Vector2 = Vector2(20, 20)
var color: Color = Color.GREEN
var texture_body: Rect2 = Rect2(Vector2(), size)

@onready var window_rect: Rect2 = get_window().get_visible_rect()


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	if position == Vector2.ZERO:
		var x: float = randf_range(0, window_rect.size.x)
		var y: float = randf_range(0, window_rect.size.y)
		position = Vector2(x, y)
	texture_body = Rect2(Vector2(-(size.x / 2), -(size.y / 2)), size)


# Called when CanvasItem has been requested to redraw.
func _draw() -> void:
	draw_rect(texture_body, color)
