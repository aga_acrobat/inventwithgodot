extends Control

enum Turns {
	player,
	ai
	}

var _turn
var _spielstatus: int = 0
var _endstatus: int
var gewinn_knoepfe: Array


onready var spielstart_hintergrund: Panel = $"%SpielstartHintergrund"
onready var beginner_hintergrund: Panel = $"%BeginnerHintergrund"
onready var beginner_textur: TextureRect = $"%BeginnerTextur"
onready var beginner_timer: Timer = $Timer
onready var spielfeld_grid: GridContainer = $"%SpielfeldGrid"
onready var spiel_knoepfe: Array = spielfeld_grid.get_children()
onready var o_textur := preload("res://assets/O.PNG")
onready var x_textur := preload("res://assets/X.PNG")
onready var spielend_hintergrund: Panel = $"%SpielEndHintergrund"
onready var gewinner_textur: TextureRect = $"%GewinnerTextur"
onready var gewinner_label: Label = $"%GewinnerLabel"
onready var schwierigkeit: OptionButton = $"%Schwierigkeit"
onready var gewinner_timer: Timer = $EndTimer
onready var blink_timer_an: Timer = $BlinkTimerAn
onready var blink_timer_aus: Timer = $BlinkTimerAus


onready var bibliothek: Libraries = Libraries.new(spiel_knoepfe)
var ai: AI


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	ready_buttons()
	randomize()
	restart()


## MAcht die Spielschalter startklar
func ready_buttons() -> void:
	# Signale der Spielknöpfe verkabeln
	for knopf in spiel_knoepfe:
		var _err = knopf.connect(
			"pressed", self, "_on_SpielKnopf_pressed", [knopf]
			)

	# Schwierigkeitsauswahl einstellen
	schwierigkeit.theme = Theme.new()
	schwierigkeit.theme.default_font = DynamicFont.new()
	schwierigkeit.theme.default_font.font_data = load(
		"res://assets/IBMPlexSans-Regular.ttf"
		)
	schwierigkeit.theme.default_font.size = 25
	var schwierigkeiten := ["schlecht", "mittel", "gut"]
	for element in schwierigkeiten:
		schwierigkeit.add_item(element)


## Resettet das Spiel und Spielfeld
func restart() -> void:
	# Gewinnergebnisse vom letzten Spiel zurücksetzen
	gewinn_knoepfe.clear()
	gewinner_textur.texture = null

	# Spielfeldknöpfe leeren: Spielzeichen entfernen, Werte zurücksetzen
	for knopf in spiel_knoepfe:
		knopf.icon = null
		knopf.disabled = false
		knopf.wert = -5

	# Spielzuganfang randomisieren
	var random := RandomNumberGenerator.new()
	var zufall := random.randi_range(0, 1)
	match zufall:
		0:
			change_turn(Turns.player)
			beginner_textur.texture = x_textur
		1:
			change_turn(Turns.ai)
			beginner_textur.texture = o_textur
	
	# startpanel zeigen
	spielstart_hintergrund.visible = true
	# beginner- und endpanel verstecken
	beginner_hintergrund.visible = false
	spielend_hintergrund.visible = false


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta: float) -> void:
	# wenn das Spiel aktiv ist und der Computer dran ist, 
	# berechnet die ai ihren Zug
	if _spielstatus == 1 and _turn == Turns.ai:
		var computer_feld = ai.calculate_move()
		if computer_feld != -1:
			var knopf = spiel_knoepfe[computer_feld]
			knopf.icon = o_textur
			knopf.wert = 0
			knopf.disabled = true
		check_endstate(Turns.player)


## Hilfsfunktion: wechselt den Spieler, der an der Reihe ist
func change_turn(new_turn) -> void:
	_turn = new_turn


## Prüft, ob das Spiel von jemandem gewonnen worden ist.
# next_turn(Turns)
func check_endstate(next_turn) -> void:
	var status: int = -1
	var gewinn_zeile: Array = []
	
	# prüft auf Unentschieden, Spielergewinn oder AI-Gewinn
	var freies_feld: int = bibliothek.find_free_field_in_board()
	var spieler_gewinn: Array = bibliothek.check_zeilen_summen(
		3, bibliothek.gewinn_zeilen
		)
	var ai_gewinn: Array = bibliothek.check_zeilen_summen(
		0, bibliothek.gewinn_zeilen
		)
	
	if spieler_gewinn:
		status = 1
		gewinn_zeile = spieler_gewinn
	elif ai_gewinn:
		status = 0
		gewinn_zeile = ai_gewinn
	elif freies_feld == -1:
		status = 2

	if status == -1:
		change_turn(next_turn)
	else:
		_spielstatus = 0
		_endstatus = status
		gewinner_timer.start()
		show_endstate(gewinn_zeile)


## Setzt das Spiel auf Ende
func show_endstate(gewinn_zeile: Array) -> void:
	if _endstatus == 0:
		gewinner_textur.texture = o_textur
		gewinner_label.visible = false
	elif _endstatus == 1:
		gewinner_textur.texture = x_textur
		gewinner_label.visible = false
	elif _endstatus == 2:
		gewinner_label.visible = true
		gewinner_textur.visible = false
	for position in gewinn_zeile:
		gewinn_knoepfe.append(spiel_knoepfe[position])
	for knopf in gewinn_knoepfe:
		knopf.icon = null
	blink_timer_an.start()


## Ändert Aussehen und Wert des Spielfeldes, wenn der Spieler einen Zug gesetzt hat
func _on_SpielKnopf_pressed(knopf) -> void:
	knopf.icon = x_textur
	knopf.wert = 1
	knopf.disabled = true
	check_endstate(Turns.ai)


## Beginnt das eigentliche Spiel und erstellt die AI mit gewählter
## Schwierigkeitseinstellung
func _on_StartButton_pressed() -> void:
	_spielstatus = 1
	spielstart_hintergrund.visible = false
	beginner_hintergrund.visible = true
	beginner_timer.start()
	ai = AI.new(spiel_knoepfe, schwierigkeit.selected)


func _on_NochEinmalButton_pressed() -> void:
	restart()


func _on_Timer_timeout() -> void:
	beginner_hintergrund.visible = false


## Wenn der Endtimer abgelaufen ist, wird das Spielergebnis gezeigt
func _on_EndTimer_timeout() -> void:
	blink_timer_an.stop()
	blink_timer_aus.stop()
	spielend_hintergrund.visible = true


func _on_BlinkTimerAn_timeout() -> void:
	for knopf in gewinn_knoepfe:
		knopf.icon = gewinner_textur.texture
	blink_timer_aus.start()


func _on_BlinkTimerAus_timeout() -> void:
	for knopf in gewinn_knoepfe:
		knopf.icon = null
	blink_timer_an.start()
