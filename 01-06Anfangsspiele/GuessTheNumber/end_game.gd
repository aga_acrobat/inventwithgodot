extends Label

var global

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	global = get_node("/root/Global")
	
	if global.endstatus == 0:
		$".".text = 'Tut mir Leid. Deine sechs Runden sind um. \rDie gesuchte Zahl war %s' % global.computerzahl
	elif global.endstatus == 1:
		$".".text = 'Super! Herzlichen Glückwunsch. \rDu hast die Zahl in %s Runden erraten.' % global.endrunde
