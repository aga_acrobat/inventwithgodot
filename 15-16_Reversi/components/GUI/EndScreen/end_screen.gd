# TODO: docs!
extends Control

@onready var counter_light: Label = $"%Light-Label"
@onready var counter_dark: Label = $"%Dark-Label"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	SignalManager.score_calculated.connect(_on_score_calculated_received)


func _on_end_button_pressed() -> void:
	get_tree().quit()


func _on_new_button_pressed() -> void:
	SignalManager.restart_pressed.emit()


func _on_score_calculated_received(light: int, dark: int) -> void:
	counter_light.text = str(light)
	counter_dark.text = str(dark)
