extends Area2D

const SPEED: int = 400

var size: Vector2 = Vector2(50, 50)
var direction: Vector2 = Vector2.ZERO

@onready var growth_factor: Vector2 = Vector2(2, 2) / ($"Sprite2D" as Sprite2D).texture.get_size()
@onready var window_rect: Rect2 = get_window().get_visible_rect()
@onready var pickup_sound: AudioStreamPlayer = $"%PickUp"


# Called when the node enters the scene tree for the first time.
# sets nodes starting position
func _ready() -> void:
	position = Vector2(300, 100)


# Called every frame. 'delta' is the elapsed time since the previous frame.
# changes direction on bounce
# moves PlayerBoxNode according to direction and speed
func _process(delta: float) -> void:
	check_bounce()
	var velocity: Vector2 = direction * SPEED
	position += velocity * delta


# catch key action events
# set movement direction
# teleports PlayerBoxNode
func _unhandled_key_input(event: InputEvent) -> void:
	if event.is_action_pressed("teleport"):
		var x: float = randf_range(0, window_rect.size.x)
		var y: float = randf_range(0, window_rect.size.y)
		position = Vector2(x, y)
	if event.is_action_pressed("ui_up"):
		direction = Vector2.UP
	elif event.is_action_pressed("ui_down"):
		direction = Vector2.DOWN
	elif event.is_action_pressed("ui_left"):
		direction = Vector2.LEFT
	elif event.is_action_pressed("ui_right"):
		direction = Vector2.RIGHT
	elif event.is_action_released("ui_right"):
		direction = Vector2.ZERO
	elif event.is_action_released("ui_up"):
		direction = Vector2.ZERO
	elif event.is_action_released("ui_left"):
		direction = Vector2.ZERO
	elif event.is_action_released("ui_down"):
		direction = Vector2.ZERO


# Triggered when an area node enters this area
# PlayerBoxNode growths on collision with food node
# Food node disappears
func _on_area_entered(area: Area2D) -> void:
	if area.is_in_group("food"):
		scale = scale + growth_factor / scale
		pickup_sound.play()
		area.queue_free()


# check if box bounces off walls
func check_bounce() -> void:
	if position.x < window_rect.position.x:
		position.x = 20
	elif position.x > window_rect.size.x:
		position.x = window_rect.size.x - 20
	if position.y < window_rect.position.y:
		position.y = 20
	elif position.y > window_rect.size.y:
		position.y = window_rect.size.y - 20
