# TODO: docs!
extends Control

var hint_names: Array = ["keine", "Felder", "Ecken", "Reihen"]

var strategy_names: Array = ["einfach", "mittel", "schwer", "extra"]

@onready var warning_label: Label = $"%WarnLabel"

@onready var counter_light: Label = $"%LightCounterLabel"
@onready var counter_dark: Label = $"%DarkCounterLabel"

@onready var light_box_label: Label = $"%LightBoxLabel"
@onready var dark_box_label: Label = $"%DarkBoxLabel"

@onready var light_cb_vbox: VBoxContainer = $"%LightCBVBox"
@onready var dark_cb_vbox: VBoxContainer = $"%DarkCBVBox"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	SignalManager.color_chosen.connect(_on_color_chosen_received)
	SignalManager.score_calculated.connect(_on_score_calculated_received)
	SignalManager.warning.connect(_on_warning_received)
	SignalManager.restart_pressed.connect(_on_restart_pressed_received)


func _on_color_chosen_received(color: int) -> void:
	match color:
		Constants.COLOR.LIGHT:
			set_light_box(Constants.ACTOR.PLAYER)
			set_light_check_buttons(Constants.ACTOR.PLAYER)
			set_dark_box(Constants.ACTOR.AI)
			set_dark_check_buttons(Constants.ACTOR.AI)
		Constants.COLOR.DARK:
			set_light_box(Constants.ACTOR.AI)
			set_light_check_buttons(Constants.ACTOR.AI)
			set_dark_box(Constants.ACTOR.PLAYER)
			set_dark_check_buttons(Constants.ACTOR.PLAYER)


func _on_score_calculated_received(light: int, dark: int) -> void:
	counter_light.text = str(light)
	counter_dark.text = str(dark)


func set_light_box(actor: int) -> void:
	match actor:
		Constants.ACTOR.PLAYER:
			light_box_label.text = "Hinweise"
		Constants.COLOR.DARK:
			light_box_label.text = "AI"


func set_dark_box(actor: int) -> void:
	match actor:
		Constants.ACTOR.PLAYER:
			dark_box_label.text = "Hinweise"
		Constants.COLOR.DARK:
			dark_box_label.text = "AI"


func set_light_check_buttons(actor: int) -> void:
	var light_cbs: Array[Node] = light_cb_vbox.get_children()
	var light_cb00: CheckBox = light_cbs[0]
	match actor:
		Constants.ACTOR.PLAYER:
			for cb_index: int in light_cbs.size():
				var cb: CheckBox = light_cbs[cb_index] as CheckBox
				cb.text = hint_names[cb_index]
				var method_name: String = "_on_hint_cb_" + str(cb_index) + "_pressed"
				var toggled_connections: Array = cb.toggled.get_connections()
				if toggled_connections:
					var connection: Dictionary = toggled_connections[0]
					var connection_callable: Callable = connection.callable
					cb.toggled.disconnect(connection_callable)
				cb.toggled.connect(Callable(self, method_name))
		Constants.ACTOR.AI:
			for cb_index: int in light_cbs.size():
				var cb: CheckBox = light_cbs[cb_index] as CheckBox
				cb.text = strategy_names[cb_index]
				var method_name: String = "_on_strategy_cb_" + str(cb_index) + "_pressed"
				var toggled_connections: Array = cb.toggled.get_connections()
				if toggled_connections:
					var connection: Dictionary = toggled_connections[0]
					var connection_callable: Callable = connection.callable
					cb.toggled.disconnect(connection_callable)
				cb.toggled.connect(Callable(self, method_name))
	light_cb00.button_pressed = true


func set_dark_check_buttons(actor: int) -> void:
	var dark_cbs: Array[Node] = dark_cb_vbox.get_children()
	var dark_cb00: CheckBox = dark_cbs[0]
	match actor:
		Constants.ACTOR.PLAYER:
			for cb_index: int in dark_cbs.size():
				var cb: CheckBox = dark_cbs[cb_index] as CheckBox
				cb.text = hint_names[cb_index]
				var method_name: String = "_on_hint_cb_" + str(cb_index) + "_pressed"
				var toggled_connections: Array = cb.toggled.get_connections()
				if toggled_connections:
					var connection: Dictionary = toggled_connections[0]
					var connection_callable: Callable = connection.callable
					cb.toggled.disconnect(connection_callable)
				cb.toggled.connect(Callable(self, method_name))
		Constants.ACTOR.AI:
			for cb_index: int in dark_cbs.size():
				var cb: CheckBox = dark_cbs[cb_index] as CheckBox
				cb.text = strategy_names[cb_index]
				var method_name: String = "_on_strategy_cb_" + str(cb_index) + "_pressed"
				var toggled_connections: Array = cb.toggled.get_connections()
				if toggled_connections:
					var connection: Dictionary = toggled_connections[0]
					var connection_callable: Callable = connection.callable
					cb.toggled.disconnect(connection_callable)
				cb.toggled.connect(Callable(self, method_name))
	dark_cb00.button_pressed = true


func _on_hint_cb_0_pressed(_toggled: bool) -> void:
	SignalManager.hints_toggled.emit(0)


func _on_hint_cb_1_pressed(_toggled: bool) -> void:
	SignalManager.hints_toggled.emit(1)


func _on_hint_cb_2_pressed(_toggled: bool) -> void:
	SignalManager.hints_toggled.emit(2)


func _on_hint_cb_3_pressed(_toggled: bool) -> void:
	SignalManager.hints_toggled.emit(3)


func _on_strategy_cb_0_pressed(_toggled: bool) -> void:
	SignalManager.strategy_toggled.emit(0)


func _on_strategy_cb_1_pressed(_toggled: bool) -> void:
	SignalManager.strategy_toggled.emit(1)


func _on_strategy_cb_2_pressed(_toggled: bool) -> void:
	SignalManager.strategy_toggled.emit(2)


func _on_strategy_cb_3_pressed(_toggled: bool) -> void:
	SignalManager.strategy_toggled.emit(3)


func _on_warning_received(warning: String) -> void:
	warning_label.text = warning
	warning_label.visible = true
	await get_tree().create_timer(2.0).timeout
	warning_label.visible = false


func _on_restart_pressed_received() -> void:
	warning_label.visible = false
	counter_dark.text = str(2)
	counter_light.text = str(2)
