class_name FoodBox

extends Area2D
#
# FoodBox
#

var color: Color = Color.GREEN

@onready var window_rect: Rect2 = get_window().get_visible_rect()


# Called when the node enters the scene tree for the first time.
# Place Node at random position inside game window, if no position given at Node creation
func _ready() -> void:
	if position == Vector2.ZERO:
		var x: float = randf_range(0, window_rect.size.x)
		var y: float = randf_range(0, window_rect.size.y)
		position = Vector2(x, y)
