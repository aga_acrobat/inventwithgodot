extends Node

var endstatus: String

var wortlaenge := [0, 0]

func set_wortlaenge(auswahl: int) -> void:
	match auswahl:
		0:
			wortlaenge[0] = 0
			wortlaenge[1] = 20
		1:
			wortlaenge[0] = 1
			wortlaenge[1] = 5
		2:
			wortlaenge[0] = 6
			wortlaenge[1] = 10
		3:
			wortlaenge[0] = 11
			wortlaenge[1] = 20
