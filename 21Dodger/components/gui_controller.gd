extends Control

@onready var game_over_label: Label = $"TitleVBoxContainer/MarginContainer/GameOver"
@onready var title_label: Label = $"TitleVBoxContainer/MarginContainer/Title"
@onready var press_key_label: Label = $"TitleVBoxContainer/PressKey"
@onready var high_score_label: Label = $"%HighScore"
@onready var current_score_label: Label = $"%CurrentScore"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	SignalManager.state_changed.connect(_on_state_changed_received)
	SignalManager.current_score_changed.connect(_on_current_score_changed_received)
	SignalManager.high_score_changed.connect(_on_high_score_changed_received)


# called when "game" state is reached
func game_play():
	press_key_label.hide()
	title_label.hide()
	game_over_label.hide()


# called when "title" state is reached
func game_title():
	press_key_label.show()
	title_label.show()
	game_over_label.hide()


# called when "over" state is reached
func game_over():
	game_over_label.show()
	press_key_label.show()


# called when the game state changes
func _on_state_changed_received(new_state):
	match new_state:
		Constants.STATE.TITLE:
			game_title()
		Constants.STATE.OVER:
			game_over()
		Constants.STATE.GAME:
			game_play()


# called when current score changes
func _on_current_score_changed_received(new_score):
	current_score_label.text = str(new_score)


# called when high score changes
func _on_high_score_changed_received(new_score):
	high_score_label.text = str(new_score)
