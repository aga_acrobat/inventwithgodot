class_name Box

extends Node2D
##
## box for 18 animation
##

const SPEED: int = 100

var size: Vector2
var color: Color
var direction: Vector2
var body: Rect2

@onready var window_rect: Rect2 = get_window().get_visible_rect()


# Called when the object's script is instantiated,
func _init(_position: Vector2, _size: Vector2, _color: Color, _direction: Vector2) -> void:
	position = _position
	size = _size
	color = _color
	direction = _direction


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	body = Rect2(Vector2(), size)


# Called when CanvasItem has been requested to redraw.
func _draw() -> void:
	draw_rect(body, color)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	move_box(delta)
	check_bounce()


# move box in direction
func move_box(delta: float) -> void:
	var velocity: Vector2 = direction * SPEED
	position += velocity * delta


# check if box bounces off walls
func check_bounce() -> void:
	if position.x < window_rect.position.x || position.x + size.x > window_rect.size.x:
		direction.x *= -1
	elif position.y < window_rect.position.y || position.y + size.y > window_rect.size.y:
		direction.y *= -1
