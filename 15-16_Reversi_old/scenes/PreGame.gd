extends Control

enum COLOR {NONE = 0, LIGHT = 1, DARK = 2}

var player_color: int
var ai_color: int

@onready var reversi_game = $".."

# Called when the node enters the scene tree for the first time.
func _ready():
	visible = true
	player_color = COLOR.LIGHT
	ai_color = COLOR.DARK


func _on_light_cb_pressed():
	player_color = COLOR.LIGHT
	ai_color = COLOR.DARK


func _on_dark_cb_pressed():
	player_color = COLOR.DARK
	ai_color = COLOR.LIGHT


func _on_start_button_pressed():
	reversi_game.start_game(player_color)
	visible = false
