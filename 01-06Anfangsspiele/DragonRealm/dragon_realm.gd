extends Control

var intro = "You are in a land full of dragons. In front of you,
you see two caves. In one cave, the dragon is friendly
and will share his treasure with you. The other dragon
is greedy and hungry, and will eat you on sight.

Which cave will you go into? (1 or 2)"

var verlauf = ["You approach the cave...",
"It is dark and spooky...",
"A large dragon jumps out in front of you! He opens his jaws and..."]

var gewonnen = "Gives you his treasure!"
var verloren = "Gobbles you down in one bite!"
var neues_spiel = "Do you want to play again? (yes or no)"


enum States {
	INTRO,
	VERLAUF,
	ENDE
}
var state = States.INTRO
var cave = 0

@onready var textfenster = %Textfenster
@onready var button1 = %Button1
@onready var button2 = %Button2

func change_state():
	match state:
		States.INTRO:
			textfenster.text = intro
			button1.text = "(1)"
			button2.text = "(2)"
			button1.disabled = false
			button2.disabled = false
		States.VERLAUF:
			button1.disabled = true
			button2.disabled = true
			button1.text = ""
			button2.text = ""
			for zeile in verlauf:
				textfenster.text = zeile
				await get_tree().create_timer(3.0).timeout
			cave = randi() % 2
			state = States.ENDE
			change_state()
		States.ENDE:
			if cave == 0:
				textfenster.text = gewonnen.to_upper()
			elif cave == 1:
				textfenster.text = verloren.to_upper()
			await get_tree().create_timer(3.0).timeout
			textfenster.text = neues_spiel
			button1.disabled = false
			button2.disabled = false
			button1.text = "(ja)"
			button2.text = "(nein)"

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	randomize()
	state = States.INTRO
	change_state()


func _process(_delta: float) -> void:
	if Input.is_action_pressed("ui_cancel"):
		get_tree().quit()


func _on_button_1_pressed() -> void:
	if state == States.INTRO:
		state = States.VERLAUF
		change_state()
	elif state == States.ENDE:
		state = States.INTRO
		change_state()


func _on_button_2_pressed() -> void:
	if state == States.INTRO:
		state = States.VERLAUF
		change_state()
	elif state == States.ENDE:
		get_tree().quit()
