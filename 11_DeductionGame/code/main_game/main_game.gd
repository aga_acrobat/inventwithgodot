extends Control

var num_digits: int
var max_guesses: int
var num_guesses: int = 0

var secret_numbers: Array
var guesses: Array

@onready var Global = get_node("/root/Global")
@onready var ClueLabel: Label = $"%ClueLabel"
@onready var Lock: HBoxContainer = $"%Lock"

# Called when the node enters the scene tree for the first time.
func _ready():
	num_digits = Global.num_digits
	max_guesses = Global.max_guesses
	create_digit_buttons()
	fill_digit_buttons()
	start_game()


func create_digit_buttons() -> void:
	for n in num_digits:
		var digit_button = OptionButton.new()
		Lock.add_child(digit_button)


func fill_digit_buttons() -> void:
	for digit_button in Lock.get_children():
		digit_button.clear()
		for n in range(0, 10):
			digit_button.add_item(str(n))


func start_game():
	ClueLabel.text = "Keine Hinweise"
	secret_numbers = randomize_digits()
	prints("zahlen: ", secret_numbers)


func randomize_digits() -> Array:
	var numbers: Array = range(10)
	numbers.shuffle()
	var random_numbers: Array = []
	for i in range(num_digits):
		random_numbers.append(numbers[i])
	return random_numbers


func _on_submit_button_pressed():
	num_guesses += 1
	guesses.clear()
	for guess in Lock.get_children():
		guesses.append(guess.selected)

	if check_win():
		win()
	elif num_guesses >= max_guesses:
		lost()
	else:
		check_clues()


func check_win() -> bool:
	if guesses == secret_numbers:
		return true
	return false


func check_clues() -> void:
	ClueLabel.text = ""
	var clue_text: Array = []

	for n in range(guesses.size()):
		if guesses[n] == secret_numbers[n]:
			clue_text.append("Fermi")
		elif guesses[n] in secret_numbers:
			clue_text.append("Pico")
	
	if clue_text.size() == 0:
		clue_text.append("Bagles")

	clue_text.sort()
	var text = ", ".join(clue_text)
	ClueLabel.text = str(num_guesses) + "/" + str(max_guesses) + " | " + text


func win() -> void:
	ClueLabel.text = "GEWONNEN!"


func lost() -> void:
	ClueLabel.text = "VERLOREN!"

