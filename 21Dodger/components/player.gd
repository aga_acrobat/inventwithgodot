extends Area2D

const START_POSITION: Vector2 = Vector2(280, 530)
const SPEED: int = 400

var current_state: int
var direction: Vector2 = Vector2.ZERO

@onready var window_rect: Rect2 = get_window().get_visible_rect()


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	SignalManager.state_changed.connect(_on_state_changed_received)


func _input(event):
	# Mouse in viewport coordinates.
	if event is InputEventMouseMotion:
		position = event.position


# Called when an InputEventKey hasn't been consumed
func _unhandled_key_input(event: InputEvent) -> void:
	if current_state == Constants.STATE.GAME:
		if event.is_action_pressed("ui_up"):
			direction = Vector2.UP
		elif event.is_action_pressed("ui_down"):
			direction = Vector2.DOWN
		elif event.is_action_pressed("ui_left"):
			direction = Vector2.LEFT
		elif event.is_action_pressed("ui_right"):
			direction = Vector2.RIGHT
		elif (
			event.is_action_released("ui_right")
			|| event.is_action_released("ui_up")
			|| event.is_action_released("ui_left")
			|| event.is_action_released("ui_down")
		):
			direction = Vector2.ZERO


# Called every frame. 'delta' is the elapsed time since the previous frame.
# changes direction on bounce
# moves PlayerBoxNode according to direction and speed
func _process(delta: float) -> void:
	if current_state == Constants.STATE.GAME:
		check_bounce()
		var velocity: Vector2 = direction * SPEED
		position += velocity * delta


# causes box to bounce off walls
func check_bounce() -> void:
	if position.x < window_rect.position.x:
		position.x = 20
	elif position.x > window_rect.size.x:
		position.x = window_rect.size.x - 20
	if position.y < window_rect.position.y:
		position.y = 20
	elif position.y > window_rect.size.y:
		position.y = window_rect.size.y - 20


# Called when another Area2D enters this Node
func _on_area_entered(area: Area2D) -> void:
	if area.is_in_group("baddies"):
		SignalManager.state_changed.emit(Constants.STATE.OVER)


# called when the game state changes
func _on_state_changed_received(new_state):
	current_state = new_state
	match new_state:
		Constants.STATE.TITLE:
			hide()
		Constants.STATE.GAME:
			position = START_POSITION
			show()
		Constants.STATE.OVER:
			hide()
