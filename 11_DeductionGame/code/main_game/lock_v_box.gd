extends Control

var digit_textures: Array
var current_texture_nr: int

@onready var Digit: TextureRect = $"%Digit"
@onready var UpArrow: TextureButton = $"%UpArrow"
@onready var DownArrow: TextureButton = $"%DownArrow"

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	load_digit_textures()
	create_arrow_textures()
	set_digit_texture(0)


# preloads digit textures
func load_digit_textures() -> void:
	for n in range(10):
		var img_path = "res://assets/zahlen/" + str(n) + ".png"
		var digit_texture: Texture2D = load(img_path)
		digit_textures.append(digit_texture)


# sets digit texture
func set_digit_texture(number: int) -> void:
	current_texture_nr = number
	Digit.texture = digit_textures[number]


# loads and sets textures for arrow buttons
func create_arrow_textures() -> void:
	var down_n: int = randi_range(1, 3)
	var img_path_dnormal: String = "res://assets/arrows/d" + str(down_n) + "-n.png"
	var img_path_dhover: String = "res://assets/arrows/d" + str(down_n) + "-h.png"
	var img_path_dpressed: String = "res://assets/arrows/d" + str(down_n) + "-p.png"
	var dnormal_texture: Texture2D = load(img_path_dnormal)
	var dhover_texture: Texture2D = load(img_path_dhover)
	var dpressed_texture: Texture2D = load(img_path_dpressed)
	DownArrow.texture_normal = dnormal_texture
	DownArrow.texture_hover = dhover_texture
	DownArrow.texture_pressed = dpressed_texture

	var up_n: int = randi_range(1, 3)
	var img_path_unormal: String = "res://assets/arrows/u" + str(up_n) + "-n.png"
	var img_path_uhover: String = "res://assets/arrows/u" + str(up_n) + "-h.png"
	var img_path_upressed: String = "res://assets/arrows/u" + str(up_n) + "-p.png"
	var unormal_texture: Texture2D = load(img_path_unormal)
	var uhover_texture: Texture2D = load(img_path_uhover)
	var upressed_texture: Texture2D = load(img_path_upressed)
	UpArrow.texture_normal = unormal_texture
	UpArrow.texture_hover = uhover_texture
	UpArrow.texture_pressed = upressed_texture


# decrements digit frame number by one
func _on_down_arrow_pressed():
	if current_texture_nr == 0:
		set_digit_texture(9)
	else:
		set_digit_texture(current_texture_nr - 1)


# increments digit frame number by one
func _on_up_arrow_pressed():
	if current_texture_nr == 9:
		set_digit_texture(0)
	else:
		set_digit_texture(current_texture_nr + 1)
