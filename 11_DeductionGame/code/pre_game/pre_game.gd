extends Control

var difficulties: Array = ["unbegrenzt", "einfach", "mittel", "schwer"]

@onready var Length: OptionButton = $"%Length"
@onready var Clues: CheckBox = $"%Clues"
@onready var Difficulty: OptionButton = $"%Difficulty"
@onready var Global = get_node("/root/Global")

# Called when the node enters the scene tree for the first time.
func _ready():
	for n in range(3, 11):
		Length.add_item(str(n))

	for difficulty in difficulties:
		Difficulty.add_item(difficulty)


func _on_start_pressed():
	var num_digits: int = Length.selected + 3
	var clues: bool = Clues.button_pressed
	Global.num_digits = num_digits
	Global.clues = clues
	Global.max_guesses = set_difficulty(Difficulty.selected)
	get_tree().change_scene_to_file("res://code/main_game.tscn")


func set_difficulty(difficulty: int) -> int:
	var max_guesses: int = 0
	match difficulty:
		0:
			max_guesses = 0
		1:
			max_guesses = Global.num_digits * 9
		2:
			max_guesses = Global.num_digits * 6
		3:
			max_guesses = Global.num_digits * 3

	return max_guesses
