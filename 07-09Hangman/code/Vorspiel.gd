extends Control

@onready var wortlaengen_auswahl: OptionButton = %WortlaengenAuswahl
@onready var Global = get_node("/root/Global")


## Wechselt zur HauptspielSzene beim drücken auf den StartKnopf
func _on_spiel_start_button_pressed() -> void:
	get_tree().change_scene_to_file("res://code/Hautpspiel.tscn")
	Global.set_wortlaenge(wortlaengen_auswahl.selected)


## Beendet das Spiel auf Esc-Taste
func _process(_delta: float) -> void:
	if Input.is_action_pressed("ui_cancel"):
		get_tree().quit()


