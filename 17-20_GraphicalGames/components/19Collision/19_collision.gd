extends Node2D
##
## World Controller Chpt 19 Collision
##

const BACKGROUND_COLOR: Color = Color.WHITE

var food_count: int = 20
var food_box_scene: PackedScene = preload("res://components/19Collision/food_box.tscn")

@onready var background_rect: Rect2 = get_window().get_visible_rect()


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	for i: int in range(food_count):
		add_child(food_box_scene.instantiate())


# Called when CanvasItem has been requested to redraw.
func _draw() -> void:
	draw_background()


# catch key events
func _unhandled_key_input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_cancel"):
		get_tree().quit()


# catch mouse input
func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("add_food"):
		var mouse_click: InputEventMouseButton = event as InputEventMouseButton
		var food_box: Area2D = food_box_scene.instantiate()
		food_box.position = mouse_click.position
		add_child(food_box)


# fills window with white color
func draw_background() -> void:
	draw_rect(background_rect, BACKGROUND_COLOR)


# adds new foodbox on timeout of FoodTimer
func _on_food_timer_timeout() -> void:
	add_child(food_box_scene.instantiate())
