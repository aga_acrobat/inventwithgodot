extends Control


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	$VBoxContainer/LineEdit.grab_focus()


func _on_line_edit_text_submitted(new_text: String) -> void:
	var global = get_node("/root/Global")
	global.spieler_name = new_text
	szenenwechsel()

func szenenwechsel():
	get_tree().change_scene_to_file("res://GuessTheNumber/hauptfenster.tscn")
