extends Area2D

const MINSIZE: float = 0.5
const MAXSIZE: float = 4
const MINSPEED: int = 10
const MAXSPEED: int = 100

var size: Vector2
var speed: int
var current_speed: int
var current_state: int = Constants.STATE.GAME
var current_direction: Vector2 = Vector2.DOWN

@onready var window_rect: Rect2 = get_window().get_visible_rect()


func _init() -> void:
	var randsize = randf_range(MINSIZE, MAXSIZE)
	size = Vector2(randsize, randsize)
	speed = randi_range(MINSPEED, MAXSPEED)
	current_speed = speed


func _ready() -> void:
	SignalManager.state_changed.connect(_on_state_changed_received)
	position.x = randf_range(window_rect.position.x + MAXSIZE / 2, window_rect.size.x - MAXSIZE / 2)
	apply_scale(size)


# Called when an InputEventKey hasn't been consumed
func _unhandled_key_input(event: InputEvent) -> void:
	if current_state == Constants.STATE.GAME:
		if event.is_action_pressed("cheat_slow"):
			current_speed = 10
		elif event.is_action_pressed("cheat_reverse"):
			current_direction = Vector2.UP
		elif event.is_action_released("cheat_slow") || event.is_action_released("cheat_reverse"):
			current_speed = speed
			current_direction = Vector2.DOWN


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if current_state == Constants.STATE.GAME:
		var velocity: Vector2 = current_direction * current_speed
		position += velocity * delta
		if position.y > window_rect.size.y:
			queue_free()


# called when the game state changes
func _on_state_changed_received(new_state):
	current_state = new_state
	if current_state == Constants.STATE.OVER:
		queue_free()
