##
## Main Controller Node
##
## Main scene
## manages basic game related code
##
extends Node

var starting_player: int


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	SignalManager.start_pressed.connect(_on_start_pressed_received)
	SignalManager.restart_pressed.connect(_on_restart_pressed_received)


## Randomizes which player starts the game
func randomize_start_player() -> int:
	var coin: int = randi_range(Constants.ACTOR.PLAYER, Constants.ACTOR.AI)
	return coin


## reacts to pressing of start-button in title screen
func _on_start_pressed_received() -> void:
	starting_player = randomize_start_player()
	SignalManager.turn_changed.emit(starting_player)


# quit game on ESC
func _unhandled_key_input(event: InputEvent) -> void:
	if event is InputEventKey:
		var keyboard_event: InputEventKey = event as InputEventKey
		if keyboard_event.pressed and keyboard_event.keycode == KEY_ESCAPE:
			get_tree().quit()


## restarts main Controller
## resets starting player
func _on_restart_pressed_received() -> void:
	starting_player = -1
