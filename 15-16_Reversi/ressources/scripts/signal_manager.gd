extends Node

signal ai_move_chosen(row: Array)
signal color_chosen(color: int)
signal endgame_reached
signal hints_toggled(level: int)
signal moves_calculated(
	player_rows: Array[Vector2i],
	ai_rows: Array[Vector2i],
	player_corners: Array[Vector2i],
	ai_corners: Array[Vector2i]
)
signal restart_pressed
signal score_calculated(light: int, dark: int)
signal start_pressed
signal strategy_toggled(level: int)
signal turn_changed(actor: int)
signal warning(text: String)
