extends Node2D

const ADDNEWBADDIERATE: float = 1.0

var current_state: int
var baddie_scene: PackedScene = preload("res://components/baddy.tscn")
var baddie_counter: float = ADDNEWBADDIERATE


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	SignalManager.state_changed.connect(_on_state_changed_received)


# Called when an InputEventKey hasn't been consumed
func _unhandled_key_input(event: InputEvent) -> void:
	if current_state == Constants.STATE.GAME:
		if event.is_action_pressed("cheat_slow") || event.is_action_pressed("cheat_reverse"):
			current_state = Constants.STATE.CHEAT
		elif event.is_action_released("cheat_slow") || event.is_action_released("cheat_reverse"):
			current_state = Constants.STATE.GAME


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if current_state == Constants.STATE.GAME:
		baddie_counter += delta
		if baddie_counter > ADDNEWBADDIERATE:
			add_child(baddie_scene.instantiate())
			baddie_counter = 0


# called when the game state changes
func _on_state_changed_received(new_state):
	current_state = new_state
	if new_state == Constants.STATE.OVER:
		baddie_counter = ADDNEWBADDIERATE
