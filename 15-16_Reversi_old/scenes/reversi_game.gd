extends Node

enum COLOR {NONE = 0, LIGHT = 1, DARK = 2}
enum TURN {PRE = 0, AI = 1, PLAYER = 2, END = 3}

var board_layer_tiles := 1
var tile_set_id := 0
var light_tile_atlas_coords := Vector2i(0, 1)
var dark_tile_atlas_coords := Vector2i(1, 1)
var hint_tile_atlas_coords := Vector2i(0, 2)

var player := {
	color = COLOR.NONE,
	atlas_coords = Vector2i(),
	score = 0,
	tiles = [],
	counter = Label
	}

var ai := {
	color = COLOR.NONE,
	atlas_coords = Vector2i(),
	score = 0,
	tiles = [],
	counter = Label
	}

var turn: int

var DIRECTIONS := [
	Vector2i(0, 1),
	Vector2i(1, 1),
	Vector2i(1, 0),
	Vector2i(1, -1),
	Vector2i(0, -1),
	Vector2i(-1, -1),
	Vector2i(-1, 0),
	Vector2i(-1, 1),
	]

@onready var PostGame: Control = $"%PostGame"

@onready var LightHintBox: MarginContainer = $"%LHintCBMargin"
@onready var DarkHintBox: MarginContainer = $"%RHintCBMargin"
@onready var LightHintLabel: Label = $"%LHintLabel"
@onready var DarkHintLabel: Label = $"%RHintLabel"
@onready var LightCounterLabel: Label =$"%LCounterLabel"
@onready var DarkCounterLabel: Label =$"%RCounterLabel"

@onready var WarningLabel: Label = $"%WarningLabel"
@onready var InfoPanel: Control =$"%InfoPanel"
@onready var InfoTimer = $InfoTimer

@onready var hint_labels = get_tree().get_nodes_in_group("hint_labels")


# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	set_turn(TURN.PRE)
	InfoPanel.visible = false


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	match (turn):
		TURN.AI:
			calculate_move()
			set_turn(TURN.PLAYER)
		TURN.END:
			show_postgame()


func start_game(color: int) -> void:
	set_turn(TURN.PRE)
	set_colors(color)
	reset_labels()
	set_starting_tiles()
	update_score()
	show_hint_box()
	draw_starting_player()


func set_colors(color: int) -> void:
	match (color):
		COLOR.LIGHT:
			player.color = COLOR.LIGHT
			player.atlas_coords = light_tile_atlas_coords
			player.counter = LightCounterLabel

			ai.color = COLOR.DARK
			ai.atlas_coords = dark_tile_atlas_coords
			ai.counter = DarkCounterLabel

		COLOR.DARK:
			player.color = COLOR.DARK
			player.atlas_coords = dark_tile_atlas_coords
			player.counter = DarkCounterLabel

			ai.color = COLOR.LIGHT
			ai.atlas_coords = light_tile_atlas_coords
			ai.counter = LightCounterLabel
	
	
func reset_labels():
	get_tree().call_group("hint_labels", "set_visible", false)
	LightHintBox.visible = false
	LightHintLabel.text = "Hinweise"

	DarkHintBox.visible= false
	DarkHintLabel.text = "Hinweise"


func set_starting_tiles():
	Spielfeld.clear_layer(1)
	Spielfeld.set_cell(board_layer_tiles, Vector2i(3, 3), tile_set_id, light_tile_atlas_coords)
	Spielfeld.set_cell(board_layer_tiles, Vector2i(4, 4), tile_set_id, light_tile_atlas_coords)
	Spielfeld.set_cell(board_layer_tiles, Vector2i(4, 3), tile_set_id, dark_tile_atlas_coords)
	Spielfeld.set_cell(board_layer_tiles, Vector2i(3, 4), tile_set_id, dark_tile_atlas_coords)


func update_score() -> void:
	player.score = Spielfeld.get_used_cells_by_id(board_layer_tiles, tile_set_id, player.atlas_coords).size()
	ai.score = Spielfeld.get_used_cells_by_id(board_layer_tiles, tile_set_id, ai.atlas_coords).size()
	player.counter.text = str(player.score)
	ai.counter.text = str(ai.score)


func show_hint_box():
	match (player.color):
		COLOR.LIGHT:
			LightHintBox.visible = true
			DarkHintLabel.text = "AI"
		COLOR.DARK:
			DarkHintBox.visible = true
			LightHintLabel.text = "AI"


func draw_starting_player():
	var coin = randi_range(1, 2)
	set_turn(coin)


func set_turn(new_turn: int):
	turn = new_turn


func calculate_move():
	# ai zug berechnen
	pass


func show_postgame():
	PostGame.visible = true


func _unhandled_input(event):
	if event is InputEventKey:
		if event.pressed and event.keycode == KEY_ESCAPE:
			get_tree().quit()
	# process Mouse click only if on game board.
	if event is InputEventMouseButton:
		if event.pressed:
			if event.button_index == 1:
				# Die Position des Mausklicks wird normalerweise im Bezug auf das 
				# Spielfenster (Viewport) angezeigt. Hier werden die Koordinaten
				# im Bezug zur Position des Spielbrettes umgerechnet.
				var click_position: Vector2 = Spielfeld.to_local(event.position)
				# Wenn die umgerechneten Koordinaten sich innerhalb des 
				# Spielbrettes befinden, wird der Mausklick weiter verarbeitet.
				if (
					click_position.x >=0
					and click_position.x <= 600
					and click_position.y >= 0
					and click_position.y <= 600
					):
					process_player_input(click_position)


func process_player_input(position: Vector2):
	var warning: String = "Ungültiger Zug"
	# Die übergebene Klickposition wird in die Koordinaten einer Kachel des 
	# Spielbrettes umgerechnet. 
	var map_tile: Vector2i = Vector2i(Spielfeld.local_to_map(position))
	# ist die angeklickte Kachel ein valider Spielzug?
	var tiles_to_turn = AI.check_valid_tile(map_tile, ai)
	if tiles_to_turn:
		turn_tiles(tiles_to_turn, player)
		update_score()
		if check_endgame():
			set_turn(TURN.END)
		else:
			set_turn(TURN.AI)
	else:
		WarningLabel.text = warning
		InfoPanel.visible = true
		InfoTimer.start()


func turn_tiles(tiles: Array[Vector2i], actor: Dictionary) -> void:
	for tile in tiles:
		Spielfeld.set_cell(board_layer_tiles, tile, tile_set_id, actor.atlas_coords)


func calculate_turn_tiles(clicked_tile: Vector2i, direction: Vector2i) -> Array:
	var tiles_to_turn: Array = []
	var distance: int = 1
	direction = direction * -1
	while true:
		#Feld vom angeklicktem Feld aus in gegebener Richtung und Entfernung
		var tile_to_check: Vector2i = clicked_tile + direction * distance
		var tile_color_coords:Vector2i = Spielfeld.get_cell_atlas_coords(
			board_layer_tiles, tile_to_check)
		# ist das Prüffeld leer?
		if tile_to_check not in Spielfeld.get_used_cells(board_layer_tiles):
			prints("leeres feld: ", tile_to_check)
			return []
		# liegt auf dem Prüffeld ein gegnerischer Stein?
		elif tile_color_coords == ai.atlas_coords:
			prints("gegner feld: ", tile_to_check)
			tiles_to_turn.append(tile_to_check)
			distance += 1
		# liegt auf dem Prüffeld ein eigener Stein?
		elif tile_color_coords == player.atlas_coords:
			prints("eigenes feld: ", tile_to_check)
			tiles_to_turn.append(tile_to_check)
			tiles_to_turn.append(clicked_tile)
			return tiles_to_turn
	return tiles_to_turn


func _on_info_timer_timeout():
	InfoPanel.visible = false

func check_endgame() -> bool:
	# alle felder sind besetzt
	if Spielfeld.get_used_cells(board_layer_tiles).size() == 64:
		return true
	# spieler/ai kann keinen regelkonformen zug setzen
	return false
