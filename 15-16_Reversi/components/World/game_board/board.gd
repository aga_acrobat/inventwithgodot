extends TileMap

var atlas_coords_light: Vector2i = Vector2i(0, 1)
var atlas_coords_dark: Vector2i = Vector2i(1, 1)
var atlas_coords_hints: Array[Vector2i] = [
	Vector2i(0, 0),
	Vector2i(1, 0),
	Vector2i(1, 1),
	Vector2i(0, 2),
	Vector2i(1, 2),
	Vector2i(0, 3),
	Vector2i(1, 3),
]

var layer_board: int = 0
var layer_hint: int = 1
var layer_token: int = 2

var marker_tile_set: int = 0
var hints_tile_set: int = 10

var color_player: int
var color_ai: int

var atlas_coords_player: Vector2i
var atlas_coords_ai: Vector2i

var current_turn: int
var hint_level: int

var board_fields: Array[Vector2i]

var rows_player: Array[Array]
var corners_player: Array
var rows_ai: Array[Array]
var corners_ai: Array
var rows_all: Array[Array]


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	board_fields = get_used_cells(layer_board)
	SignalManager.color_chosen.connect(_on_color_chosen)
	SignalManager.hints_toggled.connect(_on_hints_toggled)
	SignalManager.turn_changed.connect(_on_turn_changed)
	SignalManager.ai_move_chosen.connect(_on_ai_move_chosen)
	SignalManager.restart_pressed.connect(_on_restart_pressed_received)


# set four starting game tiles
func set_starting_tiles() -> void:
	clear_layer(layer_token)
	set_cell(layer_token, Vector2i(3, 3), marker_tile_set, atlas_coords_light)
	set_cell(layer_token, Vector2i(4, 4), marker_tile_set, atlas_coords_light)
	set_cell(layer_token, Vector2i(3, 4), marker_tile_set, atlas_coords_dark)
	set_cell(layer_token, Vector2i(4, 3), marker_tile_set, atlas_coords_dark)


# set marker colours according to token color chosen by player
func _on_color_chosen(color: int) -> void:
	match color:
		Constants.COLOR.LIGHT:
			atlas_coords_player = atlas_coords_light
			color_player = Constants.COLOR.LIGHT
			atlas_coords_ai = atlas_coords_dark
			color_ai = Constants.COLOR.DARK
		Constants.COLOR.DARK:
			atlas_coords_player = atlas_coords_dark
			color_player = Constants.COLOR.DARK
			atlas_coords_ai = atlas_coords_light
			color_ai = Constants.COLOR.LIGHT
	calculate_valid_moves()


# calculate rows and corner cells for player and ai
# which would be a valid move
func calculate_valid_moves() -> void:
	rows_all = []
	rows_player = calculate_valid_rows(atlas_coords_player, atlas_coords_ai)
	rows_ai = calculate_valid_rows(atlas_coords_ai, atlas_coords_player)
	rows_all.append(rows_player)
	rows_all.append(rows_ai)
	corners_player = calculate_corner_cells(rows_player, atlas_coords_player)
	corners_ai = calculate_corner_cells(rows_ai, atlas_coords_ai)
	set_hints()
	SignalManager.moves_calculated.emit(rows_player, rows_ai, corners_player, corners_ai)


# calculate valid cells in a corner
func calculate_corner_cells(valid_rows: Array, atlas_coords: Vector2i) -> Array:
	var corner_directions: Array[int] = [3, 7, 11, 15]
	var corner_cells: Array = []
	for row: Array in valid_rows:
		var end_cell: Vector2i = row[-1]
		for direction: int in corner_directions:
			var next_cell: Vector2i = get_neighbor_cell(end_cell, direction)
			var next_cell_atlas_coords: Vector2i = get_cell_atlas_coords(layer_token, next_cell)
			if (
				next_cell_atlas_coords != atlas_coords
				and next_cell_atlas_coords != Vector2i(-1, -1)
			):
				corner_cells.append(end_cell)
	return corner_cells


# change current actor according to turn change
func _on_turn_changed(actor: int) -> void:
	current_turn = actor


# react to player mouse clicks on game board
# show warning if cell not valid
func _unhandled_input(event: InputEvent) -> void:
	if current_turn == Constants.ACTOR.PLAYER:
		## spielbrett prüft, ob auf spielbrett gecklickt wurde
		var cell: Vector2i = check_game_board_clicked(event)
		if cell != Vector2i(-1, -1):
			## spielbrett prüft, ob das spielfeld steine wandeln würde
			var row_to_turn: Array[Vector2i] = check_valid_cell(cell, Constants.ACTOR.PLAYER)
			if row_to_turn:
				## ja: spielbrett setzt / wandelt steine
				set_move(row_to_turn, atlas_coords_player)
			else:
				## nein: spielbrett sendet warn signal
				SignalManager.warning.emit("Feld ungültig")


##
## prüft, ob das gegebene Event ein Klick auf dem Spielbrett ist. Gibt die
## Spielbrettzelle zurück.
func check_game_board_clicked(event: InputEvent) -> Vector2i:
	var cell: Vector2i = Vector2i(-1, -1)
	# process only mouse button events
	if event is InputEventMouseButton:
		var mouse_button_event: InputEventMouseButton = event as InputEventMouseButton
		# process only button presses
		if mouse_button_event.pressed:
			# process only left button presses
			if mouse_button_event.button_index == 1:
				var local_click_position: Vector2 = to_local(mouse_button_event.position)
				# process only if on game board
				if (
					local_click_position.x >= 0
					and local_click_position.x <= 600
					and local_click_position.y >= 0
					and local_click_position.y <= 600
				):
					#convert clicked position to cell on map
					cell = Vector2i(local_to_map(local_click_position))
	return cell


# checks if chosen cell is valid for given actor
func check_valid_cell(cell: Vector2i, actor: int) -> Array[Vector2i]:
	var row_to_turn: Array[Vector2i] = []
	var rows_to_check: Array[Array] = []
	match actor:
		Constants.ACTOR.PLAYER:
			rows_to_check = rows_player
		Constants.ACTOR.AI:
			rows_to_check = rows_ai
	for row: Array in rows_to_check:
		if cell == row[-1]:
			row_to_turn = row
			break
	return row_to_turn


# calculates all valid game rows for given actor
func calculate_valid_rows(target_color: Vector2i, oponent_color: Vector2i) -> Array[Array]:
	var neighbor_directions: Array[int] = [0, 3, 4, 7, 8, 11, 12, 15]
	var valid_rows: Array[Array] = []

	#alle eigenen und gegenerischen Steine
	var own_tiles: Array[Vector2i] = tiles_by_color(target_color)
	var opponent_tiles: Array[Vector2i] = tiles_by_color(oponent_color)

	#für jeden eigenen Stein prüfen
	for own_tile: Vector2i in own_tiles:
		#alle daneben liegenden fremden steine
		#alle richtungen
		for direction: int in neighbor_directions:
			#alle Nachbarzellen
			var neighbor: Vector2i = get_neighbor_cell(own_tile, direction)
			if neighbor in opponent_tiles:
				var valid_row: Array[Vector2i] = []
				var next_cell: Vector2i = get_neighbor_cell(neighbor, direction)
				valid_row.append(neighbor)
				while true:
					if next_cell not in board_fields:
						valid_row = []
						break
					elif next_cell in own_tiles:
						valid_row = []
						break
					elif next_cell in opponent_tiles:
						valid_row.append(next_cell)
						next_cell = get_neighbor_cell(next_cell, direction)
					else:
						valid_row.append(next_cell)
						break
				if valid_row:
					valid_rows.append(valid_row)
	return valid_rows


##
## returns Array of all Vector2i-cells of given color
func tiles_by_color(tile_color_coords: Vector2i) -> Array[Vector2i]:
	return get_used_cells_by_id(layer_token, marker_tile_set, tile_color_coords)


# sets token tiles for given row of cells and given color
func set_move(row: Array[Vector2i], tile_color_coords: Vector2i) -> void:
	## turn tiles
	for tile: Vector2i in row:
		set_cell(layer_token, tile, marker_tile_set, tile_color_coords)
	calculate_score()
	check_end_game()


# calculates current game score
func calculate_score() -> void:
	var light_score: int
	var dark_score: int
	light_score = get_used_cells_by_id(layer_token, marker_tile_set, atlas_coords_light).size()
	dark_score = get_used_cells_by_id(layer_token, marker_tile_set, atlas_coords_dark).size()
	SignalManager.score_calculated.emit(light_score, dark_score)


# checks if game end conditions are met
func check_end_game() -> void:
	calculate_valid_moves()
	if (
		(current_turn == Constants.ACTOR.PLAYER && rows_player)
		|| (current_turn == Constants.ACTOR.AI && rows_ai)
	):
		if current_turn == Constants.ACTOR.PLAYER:
			SignalManager.turn_changed.emit(Constants.ACTOR.AI)
		elif current_turn == Constants.ACTOR.AI:
			# prints("valid ai moves: ", rows_ai.size())
			# if rows_ai.size() < 3:
			# 	prints("ai rows: ", rows_ai)
			SignalManager.turn_changed.emit(Constants.ACTOR.PLAYER)
	else:
		current_turn = -1
		if not rows_ai:
			prints("no ai moves")
		elif not rows_player:
			prints("no player moves")
		# prints(rows_all)
		SignalManager.endgame_reached.emit()


# sets token chosen bei ai move
func _on_ai_move_chosen(row: Array) -> void:
	set_move(row, atlas_coords_ai)


# set hint level
func _on_hints_toggled(level: int) -> void:
	hint_level = level
	set_hints()


# show hints depending on chosen hint level
func set_hints() -> void:
	clear_layer(layer_hint)
	match hint_level:
		1:
			show_field_markers()
		2:
			show_corner_markers()
		3:
			show_row_markers()


# show hint markers for valid fields on game board
func show_field_markers() -> void:
	for row: Array[Vector2i] in rows_player:
		set_cell(layer_hint, row[-1], hints_tile_set, Vector2i(0, 0))


# show hint markers for valid corner fields on game board
func show_corner_markers() -> void:
	for cell: Vector2i in corners_player:
		set_cell(layer_hint, cell, hints_tile_set, Vector2i(0, 1))


# show hint markers for all valid rows with row length
func show_row_markers() -> void:
	for row: Array[Vector2i] in rows_player:
		var row_length: int = row.size()
		var last_cell: Vector2i = row[-1]
		set_cell(layer_hint, last_cell, hints_tile_set, atlas_coords_hints[row_length - 1])


# reset game board
func _on_restart_pressed_received() -> void:
	clear_layer(layer_hint)
	set_starting_tiles()
	color_player = -1
	color_ai = -1
	atlas_coords_player = Vector2i(-1, -1)
	atlas_coords_ai = Vector2i(-1, -1)
	current_turn = -1
	hint_level = 0
	rows_player = []
	corners_player = []
	rows_ai = []
	corners_ai = []
	rows_all = []
