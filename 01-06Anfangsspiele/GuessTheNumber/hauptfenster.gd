extends Control

var spieler_name: String
var computer_zahl: int
var spieler_zahl: int
var max_runden: int
var akt_runde := 0
var global

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	global = get_node("/root/Global")
	randomize()
	computer_zahl = randi() % 20 + 1
	global.computerzahl = computer_zahl
	
	max_runden = 6
	
	spieler_name = global.spieler_name
	var textzeile = 'Also ' + spieler_name + ', ich habe mir eine Zahl zwischen 1 und 20 ausgedacht. Rate mal.'
	$VBoxContainer/Ausgabe.text = textzeile
	
	$VBoxContainer/Eingabe.grab_focus()
	

func neue_Runde():
	if spieler_zahl == computer_zahl:
		gewonnen()
	elif akt_runde >= max_runden:
		verloren()
	elif computer_zahl > spieler_zahl:
		var text = '%s \rMeine Zahl ist größer. Rate noch einmal.' % spieler_zahl
		$VBoxContainer/Ausgabe.text = text
	elif computer_zahl < spieler_zahl:
		var text = '%s \rMeine Zahl ist kleiner. Rate noch einmal.' % spieler_zahl
		$VBoxContainer/Ausgabe.text = text
		
	akt_runde += 1

func _on_eingabe_text_submitted(new_text: String) -> void:
	var eingabezahl = new_text.to_int()
	spieler_zahl = eingabezahl
	$VBoxContainer/Eingabe.clear()
	neue_Runde()

func gewonnen():
	global.endrunde = akt_runde
	global.endstatus = 1
	get_tree().change_scene_to_file("res://GuessTheNumber/end_game.tscn")
	
	
func verloren():
	global.endstatus = 0
	get_tree().change_scene_to_file("res://GuessTheNumber/end_game.tscn")
