extends Node

@onready var spielfeld: TileMap = $Spielfeld
@onready var info_panel: Panel = $InfoPanel
@onready var chest_label: Label = %ChestLabel
@onready var sonar_label: Label = %SonarLabel
@onready var start_panel = $StartPanel
@onready var end_panel = $EndPanel
@onready var end_game_label = %EndGameLabel


var gui_color: Color = Color()
var game_chests: Array
var game_nr_chests: int = 3
var chests_found: int = 0
var nr_sonar: int = 20
var sonar_used: int = 0
var board_width: int
var board_height: int
var chest_tile: Dictionary = {
	"layer": 1,
	"tileset_source": 2,
	"atlas_coords": Vector2i()
	}
var sonar_tile: Dictionary = {
	"layer": 1,
	"tileset_source": 1
	}


# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	board_width = spielfeld.get_used_rect().end.x - 1
	board_height = spielfeld.get_used_rect().end.y - 1

func _process(_delta: float) -> void:
	if Input.is_action_pressed("ui_cancel"):
		get_tree().quit()


##Restarts the game.
func restart() -> void:
	spielfeld.clear_layer(1)
	sonar_used = 0
	chests_found = 0
	game_chests.clear()
	place_chests(game_nr_chests)
	gui_update()


# Called when there is an input event.
func _unhandled_input(event):
	# Mouse in viewport coordinates.
	if event is InputEventMouseButton:
		if event.pressed:
			gameflow(event.position)


## Places chests at random positions on game board.
## nr_chests: number of chests to be placed.
func place_chests(nr_chests: int) -> void:
	var n = 0
	while n < nr_chests:
		var x: int = randi_range(0, board_width)
		var y: int = randi_range(0, board_height)
		var kiste: Vector2i = Vector2i(x, y)
		if not kiste in game_chests:
			game_chests.append(kiste)
			n += 1


## Basic game loop.
func gameflow(mouse_position: Vector2) -> void:
	var local_mouse_position_vector = spielfeld.to_local(mouse_position)
	var local_map_coords = spielfeld.local_to_map(local_mouse_position_vector)

	if not check_used_cell(local_map_coords):
		sonar_used += 1
		if chest_found(local_map_coords):
			chests_found += 1
			show_chest(local_map_coords)
			remove_chest(local_map_coords)
			update_sonars()
			if all_chests_found():
				end_game(1)
			elif all_sonars_used():
				end_game(0)
		else:
			show_sonar(local_map_coords)
			if all_sonars_used():
				end_game(0)
		gui_update()


##Updates icons on all already placed sonars
func update_sonars() -> void:
	var set_sonars: Array = spielfeld.get_used_cells_by_id(1, 1)
	for sonar in set_sonars:
		show_sonar(sonar)


##Check for set sonars on coords.
func check_used_cell(coords: Vector2i) -> bool:
	var used_cells = spielfeld.get_used_cells(1)
	if coords in used_cells:
		return true
	return false


##Checks if there is a chest hidden under the given tile coordinates.
func chest_found(coords: Vector2i) -> bool:
	if coords in game_chests:
		return true
	return false


##Checks if all chests have been found.
func all_chests_found() -> bool:
	return game_chests.size() <= 0


##Checks if all sonars have been used.
func all_sonars_used() -> bool:
	return sonar_used >= nr_sonar


##Shows chest tile under given tile coordinates.
func show_chest(coords: Vector2i) -> void:
	spielfeld.set_cell(chest_tile["layer"], coords, chest_tile["tileset_source"], chest_tile["atlas_coords"])


##Removes chest from chest collection.
func remove_chest(coords: Vector2i) -> void:
	game_chests.erase(coords)


##Ends game with given status. 0: lost. 1: won.
func end_game(status: int) -> void:
	var end_game_text: String = "Du hast alle "
	if status == 1:
		end_game_label.text = end_game_text + "Kisten gefunden!"
	elif status == 0:
		end_game_label.text = end_game_text + "Sonare aufgebraucht."
	end_panel.show()


##Shows sonar tile under given tile coordinates.
func show_sonar(coords: Vector2i) -> void:
	var atlas_coords = Vector2i(0, smallest_chest_distance(coords))
	spielfeld.set_cell(1, coords, 1, atlas_coords)


##Returns smallest distance to a chest for given coordinates.
func smallest_chest_distance(coords: Vector2i) -> int:
	var smallest_distance = 100
	var sonar_distance: int = 0
	for chest in game_chests:
		var distance = round((chest - coords).length())
		if distance < smallest_distance:
			smallest_distance = distance
	
	if smallest_distance < 10:
		sonar_distance = smallest_distance

	return sonar_distance


##Updates GUI with nr of chests found and nr of sonars placed
func gui_update() -> void:
	chest_label.text = str(chests_found) + " / " + str(game_nr_chests)
	sonar_label.text = str(sonar_used) + " / " + str(nr_sonar)


func _on_info_panel_mouse_entered():
	var licht = Color(0, 0, 0, 0)
	info_panel.modulate = licht


func _on_info_panel_mouse_exited():
	var licht = Color(1, 1, 1, 1)
	info_panel.modulate = licht


func _on_start_button_pressed():
	start_panel.hide()
	restart()


func _on_restart_button_pressed():
	end_panel.hide()
	restart()
